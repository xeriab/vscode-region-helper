//
// -*- tab-width: 2; encoding: utf-8; mode: js; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: .commitlintrc.js
//

module.exports = {
	extends: ['@commitlint/config-conventional'],
	rules: {
		'body-max-length': [0],
		'footer-max-length': [0],
		'header-max-length': [0],
		'scope-enum': [2, 'always', ['deps', 'deps-dev', 'ci', 'devops', 'release']],
	},
}
