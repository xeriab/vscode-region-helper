# Contributing

The [`VSCode`](https://code.visualstudio.com/)/[`VSCodium`](https://vscodium.com/) Region Helper Folding welcomes new contributors. This document will guide you
through the process.
