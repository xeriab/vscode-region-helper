#
# -*- tab-width: 2; encoding: utf-8; mode: makefile; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# You can set these variables from the command line.
SHELL 				?= /usr/bin/sh
OS 					:= $(shell uname | tr A-Z a-z) # OS name
OS_ARCH 			:= $(shell uname -m) # OS architecture
OS_NAME 			:= $(shell uname -s) # OS name
CURRENT 			:= $(shell pwd)
BASENAME 			:= $(shell basename "${CURRENT}")
ENV 				?= env
PREFIX 				?= ~/.local
PROJECT 			:= RegionHelper
SIMPLE_NAME			:= region-helper
PROJECT_VERSION 	?= $(shell cat $(CURRENT)/VERSION)
UUID 				?= region_helper@xeriab.tuta.io
BUILD_DIR 			?= out
TESTS_DIR 			?= ./src/tests
REPOSITORY_URL 		?= https://gitlab.com/xeriab/vscode-extensions-region-helper
MSG_SRC 			= $(wildcard po/*.po)
INSTALL_NAME 		?= region_helper@xeriab.tuta.io
SOURCE_FILES 		?= ./src

IS_DARWIN :=$(filter Darwin,$(OS_NAME))
IS_LINUX  :=$(filter Linux,$(OS_NAME))

$(shell . ./.env)

export PATH := $(abspath out/):${PATH}

# The command line passed variable VERSION is used to set the version string
# in the metadata and in the generated zip-file. If no VERSION is passed, the
# current commit SHA1 is used as version number in the metadata while the
# generated zip file has no string attached.
ifdef VERSION
	VSTRING = _v$(VERSION)
else
	VERSION = $(shell git rev-parse HEAD)
	VSTRING =
endif

OUTPUT_NAME := $(SIMPLE_NAME)-$(PROJECT_VERSION).vsix

# Add the ability to override some variables
# Use with care
-include override.mk

ESC='\033'

# For text colour
RED="${ESC}[31m"
GREEN="${ESC}[32m"
YELLOW="${ESC}[33m"
BLUE="${ESC}[34m"
MAGENTA="${ESC}[35m"
CYAN="${ESC}[36m"
WHITE="${ESC}[37m"
LIGHT_BLACK="${ESC}[90m"
LIGHT_RED="${ESC}[91m"
LIGHT_GREEN="${ESC}[92m"
LIGHT_YELLOW="${ESC}[93m"
LIGHT_BLUE="${ESC}[94m"
LIGHT_MAGENTA="${ESC}[95m"
LIGHT_CYAN="${ESC}[96m"
LIGHT_WHITE="${ESC}[97m"

# For text styles
BOLD="${ESC}[01m"
FINE="${ESC}[0m"
DIM="${ESC}[2m"
RESET="${ESC}[39m"

define option
	@printf "${DIM}${LIGHT_WHITE}$(1))${FINE}${LIGHT_YELLOW}$(2)${FINE}${DIM}${DIM}$(3)${FINE}"
	@printf "\n"
endef

define desc
	@printf "%b" "${LIGHT_GREEN}›${FINE} ${LIGHT_BLUE}${1}${FINE}: ${2}"
	$(eval DESC_RES := "")
	$(foreach name, $(3), $(eval DESC_RES := $(shell printf "%b" "$(DESC_RES)${name}${DIM}, ${FINE}")))
	$(eval DESC_RES := $(shell echo "${DESC_RES}" | sed -re 's~(\s\,|\,|\,\s|\x1b\[[0-9;]*m)*$$~~g'))
	@printf "${DESC_RES}${DIM}.${FINE}\n"
endef

.PHONY: help
.DEFAULT_GOAL := help
help:
	@echo
	@echo "${DIM}${BOLD}${CYAN}${PROJECT}${FINE} ${DIM}v${PROJECT_VERSION}${FINE} ${YELLOW}Enhances the default code regions abilities of Visual Studio Code and VSCodium editors.${FINE}"
	@echo
	$(call desc,Operating System, ,$(OS_NAME))
	$(call desc,Architecture,     ,$(OS_ARCH))
	@echo
	@echo "${DIM}Please use \`make ${BOLD}<target>\` ${FINE}${DIM}where ${BOLD}<target> ${FINE}${DIM}is one of the following:${FINE}"
	@echo
	$(call option,1, build,         Build project.)
	$(call option,2, install-deps,  Install project dependencies.)
	$(call option,3, test,          Run tests.)
	$(call option,4, clean,         Remove all project build/tests/coverage artifacts.)
	$(call option,5, list,          List all make targets.)
	$(call option,6, lint,          Lint project.)
	$(call option,7, publish,       Publish project.)
	$(call option,8, watch,         Watch project.)
	@echo

.PHONY: clean
clean: ## Remove all build, test, coverage and artifacts.
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Cleaning Project${FINE}"
	@rm -rf $(OUTPUT_NAME)
	@rm -rf releases
	@rm -rf $(BUILD_DIR)
	@gulp clean

.PHONY: publish
publish: ## Publish project.
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Publishing Project${FINE}"
#	@vsce publish --no-yarn
	@vsce publish

.PHONY: build
build: clean ## Build project.
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Building Project${FINE}"
# @npx ttsc --pretty -p ./tsconfig.build.json
# @tsc --pretty -p ./tsconfig.build.json
# @yarn run vscode:prepublish
# @vsce package --no-yarn
# @vsce package
	@gulp package

.PHONY: test
test: ## Run tests.
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Running Tests${FINE}"
# @yarn run vscode:prepublish
	@npm run vscode:prepublish
	@node ./$(BUILD_DIR)/tests/runTest.js

.PHONY: install-deps
install-deps: ## Install project dependencies.
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Installing Project's Dependencies${FINE}"
	@npm install
# @yarn install
# @npm install

.PHONY: lint
lint:
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Linting Project${FINE}"
	@eslint . --ext .ts,.tsx,.js,.jsx

.PHONY: watch
watch:
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Watching Project${FINE}"
	@tsc --pretty -watch -p ./
# @npx ttsc --pretty -watch -p ./

# Add custom targets here
-include custom.mk

.PHONY: list
list: ## List all make targets
	@echo "${LIGHT_YELLOW}›${FINE} ${DIM}${LIGHT_WHITE}Listing All Project Make Targets${FINE}"
	@${MAKE} -pRrn : -f $(MAKEFILE_LIST) 2>/dev/null | awk -v RS= -F':' '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | grep -v -e '^[^[:alnum:]]' -e '^$@$$' | sort

# Variable outputting/exporting rules
var-%: ; @echo $($*)
varexport-%: ; @echo $*=$($*)

#.PHONY: all help build clean install-deps test lint format
.PHONY: help build clean install-deps test lint
