<center>

![Region Helper](https://gitlab.com/xeriab/vscode-region-helper/-/raw/main/media/extension-icon.png)

</center>

# [VSCode](https://code.visualstudio.com/) or [VSCodium](https://vscodium.com/) Region Helper

This extension enhances the default code regions abilities of [`VSCode`](https://code.visualstudio.com/) and [`VSCodium`](https://vscodium.com/) editors.
