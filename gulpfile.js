//
// -*- tab-width: 2; encoding: utf-8; mode: js; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: gulpfile.js
//

// @ts-check

'use strict'

//
// #region Imports
//

/* eslint-disable @typescript-eslint/no-var-requires */

const gulp = require('gulp')
const ts = require('gulp-typescript')
const typescript = require('typescript')
const sourcemaps = require('gulp-sourcemaps')
const del = require('del')
const es = require('event-stream')
const vsce = require('vsce')
const nls = require('vscode-nls-dev')
// const path = require('path')
// const runSequence = require('run-sequence')

/* eslint-enable @typescript-eslint/no-var-requires */

//
// #endregion Imports
//

//
// #region Common
//

// const tsProject = ts.createProject('./tsconfig.json', { typescript })
const tsProject = ts.createProject('./tsconfig.build.json', { typescript })

const INLINE_MAP = true

const INLINE_SOURCE = false

const BUILD_DIR = 'out'

const LOCALE_DIR = 'i18n'

const SOURCE_DIR = 'src'

// If all VSCode langaues are supported, you can use nls.coreLanguages
const LANGUAGES = [
    {
        folderName: 'ara',
        id: 'ar'
    },
    {
        folderName: 'eng',
        id: 'en'
    }
]

//
// #endregion Common
//

//
// #region Tasks
//

const cleanTask = () => del([
    'out/**',
    'package.nls.*.json',
    'region-helper*.vsix'
])

const internalCompileTask = () => doCompile(false)

const internalNlsCompileTask = () => doCompile(true)

const addI18nTask = function () {
    return gulp.src(['package.nls.json'])
        .pipe(nls.createAdditionalLanguageFiles(LANGUAGES, LOCALE_DIR))
        .pipe(gulp.dest('.'))
}

const buildTask = gulp.series(cleanTask, internalNlsCompileTask, addI18nTask)

/**
 * @param {boolean} buildNls
 */
const doCompile = function (buildNls) {
    let result = tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject()).js
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        .pipe(buildNls ? nls.rewriteLocalizeCalls() : es.through())
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        .pipe(buildNls ? nls.createAdditionalLanguageFiles(LANGUAGES, LOCALE_DIR, `${BUILD_DIR}`) : es.through())

    if (INLINE_MAP && INLINE_SOURCE) {
        result = result.pipe(sourcemaps.write())
    } else {
        result = result.pipe(sourcemaps.write(`../${BUILD_DIR}`, {
            // no inlined source
            includeContent: INLINE_SOURCE,
            // Return relative source map root directories per file.
            sourceRoot: `../${SOURCE_DIR}`
        }))
    }

    return result.pipe(gulp.dest(BUILD_DIR))
}

const vscePublishTask = () => vsce.publish()

const vscePackageTask = () => vsce.createVSIX()

//
// #endregion Tasks
//

//
// #region Gulp Tasks
//

gulp.task('default', buildTask)
gulp.task('clean', cleanTask)
gulp.task('compile', gulp.series(cleanTask, internalCompileTask))
gulp.task('build', buildTask)
gulp.task('publish', gulp.series(buildTask, vscePublishTask))
gulp.task('package', gulp.series(buildTask, vscePackageTask))

//
// #endregion Gulp Tasks
//
