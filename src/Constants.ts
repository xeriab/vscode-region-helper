//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: Constants.ts
//

'use strict'

/**
 * Extension name.
 *
 * @constant EXTENSION_NAME
 */
export const EXTENSION_NAME = `RegionHelper`

/**
 * Extension namespace.
 *
 * @constant EXTENSION_NAMESPACE
 */
export const EXTENSION_NAMESPACE = `region-helper`

/**
 * Extension simple name.
 *
 * @constant SIMPLE_NAME
 */
export const SIMPLE_NAME = `region-helper`

/**
 * End of line.
 *
 * @constant EOL
 */
export const EOL = `\n`

/**
 * End of line (Windows).
 *
 * @constant EOL_CRLF
 */
export const EOL_CRLF = `\r\n`

/**
 * End of line (Nix* Like).
 *
 * @constant EOL_LF
 */
export const EOL_LF = `\n`

/**
 * End of line.
 *
 * @constant EMPTY_STR
 */
export const EMPTY_STR = ``

/**
 * True.
 *
 * @constant TRUE
 */
export const TRUE = true

/**
 * False.
 *
 * @constant FALSE
 */
export const FALSE = false

/**
 * Null.
 *
 * @constant NULL
 */
export const NULL = null

/**
 * Null character.
 *
 * @constant NULL_CHAR
 */
export const NULL_CHAR = `\0`

/**
 * CSS color names.
 *
 * @constant CSS_COLOR_NAMES
 */
export const CSS_COLOR_NAMES = [
	'AliceBlue',
	'AntiqueWhite',
	'Aqua',
	'Aquamarine',
	'Azure',
	'Beige',
	'Bisque',
	'Black',
	'BlanchedAlmond',
	'Blue',
	'BlueViolet',
	'Brown',
	'BurlyWood',
	'CadetBlue',
	'Chartreuse',
	'Chocolate',
	'Coral',
	'CornflowerBlue',
	'Cornsilk',
	'Crimson',
	'Cyan',
	'DarkBlue',
	'DarkCyan',
	'DarkGoldenRod',
	'DarkGray',
	'DarkGrey',
	'DarkGreen',
	'DarkKhaki',
	'DarkMagenta',
	'DarkOliveGreen',
	'DarkOrange',
	'DarkOrchid',
	'DarkRed',
	'DarkSalmon',
	'DarkSeaGreen',
	'DarkSlateBlue',
	'DarkSlateGray',
	'DarkSlateGrey',
	'DarkTurquoise',
	'DarkViolet',
	'DeepPink',
	'DeepSkyBlue',
	'DimGray',
	'DimGrey',
	'DodgerBlue',
	'FireBrick',
	'FloralWhite',
	'ForestGreen',
	'Fuchsia',
	'Gainsboro',
	'GhostWhite',
	'Gold',
	'GoldenRod',
	'Gray',
	'Grey',
	'Green',
	'GreenYellow',
	'HoneyDew',
	'HotPink',
	'IndianRed',
	'Indigo',
	'Ivory',
	'Khaki',
	'Lavender',
	'LavenderBlush',
	'LawnGreen',
	'LemonChiffon',
	'LightBlue',
	'LightCoral',
	'LightCyan',
	'LightGoldenRodYellow',
	'LightGray',
	'LightGrey',
	'LightGreen',
	'LightPink',
	'LightSalmon',
	'LightSeaGreen',
	'LightSkyBlue',
	'LightSlateGray',
	'LightSlateGrey',
	'LightSteelBlue',
	'LightYellow',
	'Lime',
	'LimeGreen',
	'Linen',
	'Magenta',
	'Maroon',
	'MediumAquaMarine',
	'MediumBlue',
	'MediumOrchid',
	'MediumPurple',
	'MediumSeaGreen',
	'MediumSlateBlue',
	'MediumSpringGreen',
	'MediumTurquoise',
	'MediumVioletRed',
	'MidnightBlue',
	'MintCream',
	'MistyRose',
	'Moccasin',
	'NavajoWhite',
	'Navy',
	'OldLace',
	'Olive',
	'OliveDrab',
	'Orange',
	'OrangeRed',
	'Orchid',
	'PaleGoldenRod',
	'PaleGreen',
	'PaleTurquoise',
	'PaleVioletRed',
	'PapayaWhip',
	'PeachPuff',
	'Peru',
	'Pink',
	'Plum',
	'PowderBlue',
	'Purple',
	'RebeccaPurple',
	'Red',
	'RosyBrown',
	'RoyalBlue',
	'SaddleBrown',
	'Salmon',
	'SandyBrown',
	'SeaGreen',
	'SeaShell',
	'Sienna',
	'Silver',
	'SkyBlue',
	'SlateBlue',
	'SlateGray',
	'SlateGrey',
	'Snow',
	'SpringGreen',
	'SteelBlue',
	'Tan',
	'Teal',
	'Thistle',
	'Tomato',
	'Turquoise',
	'Violet',
	'Wheat',
	'White',
	'WhiteSmoke',
	'Yellow',
	'YellowGreen',
]
