//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: IVSCodeCommands.ts
//

'use strict'

/**
 * @interface IRegionDelimiter
 *
 * VSCode Region Delimiter Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface IRegionDelimiter {
	start: string
	end: string
}

/**
 * @interface ILanguageDelimitersConfig
 *
 * VSCode Language Delimiters Config Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface ILanguageDelimitersConfig {
	[key: string]: IRegionDelimiter
}
