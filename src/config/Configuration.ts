//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: Configuration.ts
//

//
// #region Imports
//

'use strict'

import { ExtensionContext, TextEditor, window, workspace } from 'vscode'
import * as config from './IConfiguration'
import * as defaultConfig from './DefaultConfiguration'
import * as Consts from '../Constants'

//
// #endregion Imports
//

//
// region ConfigurationService
//

/**
 * @class ConfigurationService
 *
 * Configuration Service Class.
 *
 * @since 0.1.0
 *
 * @api
 */
export class ConfigurationService {
	/**
	 * Supported languages.
	 *
	 * @private
	 * @type {String[]}
	 * @memberof ConfigurationService
	 */
	private mSupportedLanguages: string[] = []

	/**
	 * On Configuration Changed Event.
	 *
	 * @public
	 * @type {(Function | null)}
	 * @memberof ConfigurationService
	 */
	public onConfigurationChanged: (() => void) | null = null

	/**
	 * Constructor.
	 *
	 * @param {ExtensionContext} context VSCode extension context
	 *
	 * @since 0.1.0
	 */
	constructor(context: ExtensionContext) {
		context.subscriptions.push(
			workspace.onDidChangeConfiguration(e => {
				this.raiseConfigurationChanged()
			})
		)
	}

	/**
	 *
	 */
	protected raiseConfigurationChanged() {
		if (this.onConfigurationChanged) {
			this.onConfigurationChanged()
		}

		// foldingRangeProvider.configuration = loadConfiguration()
	}

	/**
	 *
	 */
	public getSupportedLanguages() {
		// const supportedLanguages: string[] = []
		const configuration = this.loadConfiguration()

		// for (const prop in configuration) {
		// 	if (prop.startsWith('[') && prop.endsWith(']')) {
		// 		const languageName = prop.substr(1, prop.length - 2)

		// 		if (!configuration.languages[prop].disableFolding) {
		// 			supportedLanguages.push(languageName)
		// 		}
		// 	}
		// }

		for (const language in configuration.languages) {
			if (language.startsWith('[') && language.endsWith(']')) {
				const languageName = language.substr(1, language.length - 2)

				if (!configuration.languages[language].disableFolding) {
					// supportedLanguages.push(languageName)
					this.mSupportedLanguages.push(languageName)
				}
			}
		}

		// return supportedLanguages
		return this.mSupportedLanguages
	}

	/**
	 *
	 */
	public loadConfiguration() {
		const loadedConfig = workspace
			.getConfiguration()
			.get<config.IConfiguration>(Consts.EXTENSION_NAMESPACE)

		let config: config.IConfiguration = Object.assign(
			{},
			defaultConfig.DefaultConfiguration
		)

		config = Object.assign(config, loadedConfig)

		return config
	}

	/**
	 * Get configuration for specified/given language.
	 *
	 * @param {String} languageId The language Id to get configuration for.
	 *
	 * @return {config.ILanguageConfiguration?} - The language configuration.
	 *
	 * @public
	 *
	 * @since 0.1.0
	 */
	public getConfigurationForLanguage(languageId: string): config.ILanguageConfiguration | null {
		const config = this.loadConfiguration()

		const currentLanguageConfig = config.languages['[' + languageId + ']']

		if ((typeof currentLanguageConfig === 'undefined') || !currentLanguageConfig) {
			return null
		}

		return currentLanguageConfig
	}

	/**
	 * Get configuration for currnet language.
	 *
	 * @param {String} languageId The language Id to get configuration for.
	 *
	 * @return {config.ILanguageConfiguration?} - The language configuration.
	 *
	 * @public
	 *
	 * @since 0.1.0
	 */
	public getConfigurationForCurrentLanguage(languageId: string): config.ILanguageConfiguration | null {
		/** @property {config.ILanguageConfiguration|null} retVal */
		let retVal: config.ILanguageConfiguration | null = null

		const config = this.loadConfiguration()

		if (window.activeTextEditor === null) {
			retVal = null
		}

		//
		// region Get the configuration for the current language
		//

		if (!languageId) {
			const activeTextEditor: TextEditor | undefined = window.activeTextEditor

			if (!activeTextEditor) {
				retVal = null
			}

			if (activeTextEditor !== undefined) {
				languageId = activeTextEditor.document.languageId
			}
		}

		const currentLanguageConfig = config.languages['[' + languageId + ']']

		if (
			typeof currentLanguageConfig === 'undefined' ||
			!currentLanguageConfig
		) {
			window.showInformationMessage(
				'Region Helper. No region folding available for language "' +
				languageId +
				'". Check that you have the language extension installed for these files.'
			)

			retVal = null
		}

		//
		// endregion Get the configuration for the current language
		//

		retVal = currentLanguageConfig

		return retVal
	}
}

//
// endregion ConfigurationService
//



