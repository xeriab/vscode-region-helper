//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: DefaultConfiguration.ts
//

//
// region Imports
//

'use strict'

import { IConfiguration } from './IConfiguration'

//
// endregion Imports
//

/**
 * @property DefaultConfiguration
 *
 * Default Configuration.
 *
 * @since 0.1.0
 *
 * @type {IConfiguration} - DefaultConfiguration
 * @api
 */
export const DefaultConfiguration: IConfiguration = {
	autoFoldAllRegions: false,
	autoRefresh: false,
	enableColoring: false,
	enableRegionExplorer: false,
	enableRegionViewer: false,
	languages: {
		'[html]': {
			foldEnd: '<!-- #endregion [NAME] -->',
			foldEndRegex: '\\<!--[\\s]*#endregion|\\<!--[\\s]*endregion',
			foldStart: '<!-- #region [NAME] -->',
			foldStartRegex: '\\<!--[\\s]*#region[\\s]*(.*)|\\<!--[\\s]*region[\\s]*(.*)'
		},

		'[xml]': {
			foldEnd: '<!-- #endregion [NAME] -->',
			foldEndRegex: '\\<!--[\\s]*#endregion|\\<!--[\\s]*endregion',
			foldStart: '<!-- #region [NAME] -->',
			foldStartRegex: '\\<!--[\\s]*#region[\\s]*(.*)|\\<!--[\\s]*region[\\s]*(.*)'
		},

		'[vue]': {
			foldEnd: '<!-- #endregion [NAME] -->',
			foldEndRegex: '\\<!--[\\s]*#endregion|\\<!--[\\s]*endregion',
			foldStart: '<!-- #region [NAME] -->',
			foldStartRegex: '\\<!--[\\s]*#region[\\s]*(.*)|\\<!--[\\s]*region[\\s]*(.*)'
		},

		'[lua]': {
			foldEnd: '--#endregion [NAME]',
			foldEndRegex: '--[\\s]*#endregion',
			foldStart: ' --#region [NAME]',
			foldStartRegex: '--[\\s]*#region[\\s]*(.*)'
		},

		'[markdown]': {
			foldEnd: '<!-- #endregion [NAME] -->',
			foldEndRegex: '\\<!--[\\s]*#endregion',
			foldStart: '<!-- #region [NAME] -->',
			defaultFoldStartRegex: '\\<!--[\\s]*#region\\(collapsed\\)[\\s]*(.*)|\\<!--[\\s]*region\\(collapsed\\)[\\s]*(.*)',
			foldStartRegex: '\\<!--[\\s]*#region[\\s]*(.*)|\\<!--[\\s]*region[\\s]*(.*)'
		},

		'[python]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*#endregion|[\\s]*#[\\s]*endregion',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[ruby]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*#endregion|[\\s]*#[\\s]*endregion',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[r]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*#endregion|[\\s]*#[\\s]*endregion',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[rust]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[typescript]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[typescriptreact]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[javascript]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[javascriptreact]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[java]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[kotlin]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[dart]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[hls]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[hcl]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[hocon]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[conf]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[go]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[php]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[json]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[jsonc]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[swift]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[csharp]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[cs]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[c++]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[d]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[gradle]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[groovy]': {
			// foldEnd: '/* #endregion */',
			// foldEndRegex: '/\\*[\\s]*#endregion',
			// foldStart: '/* #region [NAME] */',
			// foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
			foldEnd: '// endregion [NAME]',
			foldEndRegex: '\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*#endregion[\\s]*(.*)[\\s]*|\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*|\\/\\/\\/[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '// region [NAME]',
			foldStartRegex: '^\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*#region[\\s]*(.*)[\\s]*$|^\\/\\/\\/[\\s]*region[\\s]*(.*)[\\s]*$'
		},

		'[sass]': {
			foldEnd: '/* #endregion [NAME] */',
			foldEndRegex: '/\\*[\\s]*#endregion[\\s]*(.*)[\\s]*|/\\*[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '/* #region [NAME] */',
			foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$|^[\\s]*/\\*[\\s]*region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
		},

		'[scss]': {
			foldEnd: '/* #endregion [NAME] */',
			foldEndRegex: '/\\*[\\s]*#endregion[\\s]*(.*)[\\s]*|/\\*[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '/* #region [NAME] */',
			foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$|^[\\s]*/\\*[\\s]*region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
		},

		'[css]': {
			foldEnd: '/* #endregion [NAME] */',
			foldEndRegex: '/\\*[\\s]*#endregion[\\s]*(.*)[\\s]*|/\\*[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '/* #region [NAME] */',
			foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$|^[\\s]*/\\*[\\s]*region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
		},

		'[less]': {
			foldEnd: '/* #endregion [NAME] */',
			foldEndRegex: '/\\*[\\s]*#endregion[\\s]*(.*)[\\s]*|/\\*[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '/* #region [NAME] */',
			foldStartRegex: '^[\\s]*/\\*[\\s]*#region[\\s]*(.*)[\\s]*\\*/[\\s]*$|^[\\s]*/\\*[\\s]*region[\\s]*(.*)[\\s]*\\*/[\\s]*$'
		},

		// '[twig]': {
		// 	foldEnd: '<!-- #endregion -->',
		// 	foldEndRegex: '\\<!--[\\s]*#endregion',
		// 	foldStart: '<!-- #region [NAME] -->',
		// 	foldStartRegex: '\\<!--[\\s]*#region[\\s]*(.*)'
		// },

		'[sql]': {
			foldEnd: '-- #endregion [NAME]',
			foldEndRegex: '[\\s]*--[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*--[\\s]*#endregion[\\s]*(.*)[\\s]*|--[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '-- #region [NAME]',
			foldStartRegex: '^[\\s]*--[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*--[\\s]*#region[\\s]*(.*)[\\s]*$|^--[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[env]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[dotenv]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[makefile]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[makefile2]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[editorconfig]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[gitconfig]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[gitignore]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[ignore]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[properties]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[shellscript]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[bash]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[fish]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[zsh]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[toml]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[yaml]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[desktop]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[nginx]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[apache]': {
			foldEnd: '# endregion [NAME]',
			foldEndRegex: '[\\s]*#[\\s]*endregion[\\s]*(.*)[\\s]*|[\\s]*#[\\s]*#endregion[\\s]*(.*)[\\s]*|#[\\s]*#endregion[\\s]*(.*)[\\s]*',
			foldStart: '# region [NAME]',
			foldStartRegex: '^[\\s]*#[\\s]*region[\\s]*(.*)[\\s]*$|^[\\s]*#[\\s]*#region[\\s]*(.*)[\\s]*$|^#[\\s]*#region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[ini]': {
			foldEnd: '; endregion [NAME]',
			foldEndRegex: '[\\s]*\\;[\\s]*#endregion[\\s]*(.*)[\\s]*|\\;[\\s]*#endregion[\\s]*(.*)[\\s]*|[\\s]*\\;[\\s]*endregion[\\s]*(.*)[\\s]*|\\;[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '; region [NAME]',
			foldStartRegex: '^[\\s]*\\;[\\s]*#region[\\s]*(.*)[\\s]*$|^\\;[\\s]*#region[\\s]*(.*)[\\s]*$|^[\\s]*\\;[\\s]*region[\\s]*(.*)[\\s]*$|^\\;[\\s]*region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[ahk]': {
			foldEnd: '; endregion [NAME]',
			foldEndRegex: '[\\s]*\\;[\\s]*#endregion[\\s]*(.*)[\\s]*|\\;[\\s]*#endregion[\\s]*(.*)[\\s]*|[\\s]*\\;[\\s]*endregion[\\s]*(.*)[\\s]*|\\;[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '; region [NAME]',
			foldStartRegex: '^[\\s]*\\;[\\s]*#region[\\s]*(.*)[\\s]*$|^\\;[\\s]*#region[\\s]*(.*)[\\s]*$|^[\\s]*\\;[\\s]*region[\\s]*(.*)[\\s]*$|^\\;[\\s]*region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},

		'[ansible]': {
			foldEnd: '; endregion [NAME]',
			foldEndRegex: '[\\s]*\\;[\\s]*#endregion[\\s]*(.*)[\\s]*|\\;[\\s]*#endregion[\\s]*(.*)[\\s]*|[\\s]*\\;[\\s]*endregion[\\s]*(.*)[\\s]*|\\;[\\s]*endregion[\\s]*(.*)[\\s]*',
			foldStart: '; region [NAME]',
			foldStartRegex: '^[\\s]*\\;[\\s]*#region[\\s]*(.*)[\\s]*$|^\\;[\\s]*#region[\\s]*(.*)[\\s]*$|^[\\s]*\\;[\\s]*region[\\s]*(.*)[\\s]*$|^\\;[\\s]*region[\\s]*(.*)[\\s]*$',
			isFoldedByDefault: false,
			disableFolding: false
		},
	},
}
