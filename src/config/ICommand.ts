//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: IVSCodeCommands.ts
//

'use strict'

/**
 * @interface ICommand
 *
 * VSCode Command Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface ICommand {
	name: string
	description?: string
	action(parameter?: any): void
}

/**
 * @type {ICommandArray}
 *
 * VSCode Command Array Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type ICommandArray = Array<ICommand>
