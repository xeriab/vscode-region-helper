//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: IConfiguration.ts
//

'use strict'

/**
 * @interface IFoldConfiguration
 *
 * Fold Configuration Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface IFoldConfiguration {
	foldEnd: string
	foldEndRegex: string
	foldStart: string
	foldStartRegex: string
	isFoldedByDefault?: boolean
}

/**
 * @interface ILanguageConfiguration
 *
 * Language Configuration Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface ILanguageConfiguration extends IFoldConfiguration {
	defaultFoldStartRegex?: string
	disableFolding?: boolean
	// Latest version
	foldDefinitions?: IFoldConfiguration[]
}

/**
 * @interface IConfiguration
 *
 * Configuration Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface IConfiguration extends Object {
	autoFoldAllRegions?: boolean
	autoRefresh?: boolean
	enableColoring?: boolean
	enableRegionExplorer?: boolean
	enableRegionViewer?: boolean
	languages: {
		[languageName: string]: ILanguageConfiguration
	}
	// [languageName: string]: ILanguageConfiguration
}

/**
 * @interface IOptionsConfiguration
 *
 * Options Configuration Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface IOptionsConfiguration {
	collapseDefaultRegionsOnOpen: boolean
}

/**
 * @interface DefaultOptionsConfiguration
 *
 * Default Configuration Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export const DefaultOptionsConfiguration: IOptionsConfiguration = {
	collapseDefaultRegionsOnOpen: true
}

/**
 * Represents the configuration. It is a merged view of
 *
 * - *Default Settings*
 * - *Global (User) Settings*
 * - *Workspace settings*
 * - *Workspace Folder settings* - From one of the [Workspace Folders](#workspace.workspaceFolders) under which requested resource belongs to.
 * - *Language settings* - Settings defined under requested language.
 *
 * The *effective* value (returned by [`get`](#WorkspaceConfiguration.get)) is computed by overriding or merging the values in the following order.
 *
 * ```
 * `defaultValue` (if defined in `package.json` otherwise derived from the value's type)
 * `globalValue` (if defined)
 * `workspaceValue` (if defined)
 * `workspaceFolderValue` (if defined)
 * `defaultLanguageValue` (if defined)
 * `globalLanguageValue` (if defined)
 * `workspaceLanguageValue` (if defined)
 * `workspaceFolderLanguageValue` (if defined)
 * ```
 * **Note:** Only `object` value types are merged and all other value types are overridden.
 *
 * Example 1: Overriding
 *
 * ```ts
 * defaultValue = 'on';
 * globalValue = 'relative'
 * workspaceFolderValue = 'off'
 * value = 'off'
 * ```
 *
 * Example 2: Language Values
 *
 * ```ts
 * defaultValue = 'on';
 * globalValue = 'relative'
 * workspaceFolderValue = 'off'
 * globalLanguageValue = 'on'
 * value = 'on'
 * ```
 *
 * Example 3: Object Values
 *
 * ```ts
 * defaultValue = { "a": 1, "b": 2 };
 * globalValue = { "b": 3, "c": 4 };
 * value = { "a": 1, "b": 3, "c": 4 };
 * ```
 *
 * *Note:* Workspace and Workspace Folder configurations contains `launch` and `tasks` settings. Their basename will be
 * part of the section identifier. The following snippets shows how to retrieve all configurations
 * from `launch.json`:
 *
 * ```ts
 * // launch.json configuration
 * const config = workspace.getConfiguration('launch', vscode.workspace.workspaceFolders[0].uri);
 *
 * // retrieve values
 * const values = config.get('configurations');
 * ```
 */
export interface IExtensionConfiguration {
	/**
	 * Return a value from this configuration.
	 *
	 * @param section Configuration name, supports _dotted_ names.
	 * @return The value `section` denotes or `undefined`.
	 */
	get<T>(section: string): T | undefined

	/**
	 * Return a value from this configuration.
	 *
	 * @param section Configuration name, supports _dotted_ names.
	 * @param defaultValue A value should be returned when no value could be found, is `undefined`.
	 * @return The value `section` denotes or the default.
	 */
	get<T>(section: string, defaultValue: T): T

	/**
	 * Check if this configuration has a certain value.
	 *
	 * @param section Configuration name, supports _dotted_ names.
	 * @return `true` if the section doesn't resolve to `undefined`.
	 */
	has(section: string): boolean

	/**
	 * Retrieve all information about a configuration setting. A configuration value
	 * often consists of a *default* value, a global or installation-wide value,
	 * a workspace-specific value, folder-specific value
	 * and language-specific values (if [WorkspaceConfiguration](#WorkspaceConfiguration) is scoped to a language).
	 *
	 * Also provides all language ids under which the given configuration setting is defined.
	 *
	 * *Note:* The configuration name must denote a leaf in the configuration tree
	 * (`editor.fontSize` vs `editor`) otherwise no result is returned.
	 *
	 * @param section Configuration name, supports _dotted_ names.
	 * @return Information about a configuration setting or `undefined`.
	 */
	inspect<T>(section: string): {
		key: string

		defaultValue?: T
		globalValue?: T
		workspaceValue?: T,
		workspaceFolderValue?: T,

		defaultLanguageValue?: T
		globalLanguageValue?: T
		workspaceLanguageValue?: T
		workspaceFolderLanguageValue?: T

		languageIds?: string[]

	} | undefined

	/**
	 * Readable dictionary that backs this configuration.
	 */
	readonly [key: string]: any
}
