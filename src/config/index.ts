//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Exports
//

'use strict'

export * from './Configuration'
export * from './DefaultConfiguration'
export * from './IConfiguration'
export * from './ICommand'
export * as Configuration from './Configuration'
export { ConfigurationService } from './Configuration'
export { DefaultConfiguration } from './DefaultConfiguration'
export {
	IFoldConfiguration,
	ILanguageConfiguration,
	IConfiguration,
	IOptionsConfiguration,
	DefaultOptionsConfiguration,
	IExtensionConfiguration,
} from './IConfiguration'
export { ICommand, ICommandArray } from './ICommand'
export { ILanguageDelimitersConfig, IRegionDelimiter } from './Common'

//
// endregion Exports
//
