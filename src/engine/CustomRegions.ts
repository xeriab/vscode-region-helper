//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: CustomRegions.ts
//

//
// region Imports
//

'use strict'

import { Position, TextDocument } from 'vscode'
import {
	ConfigurationService,
	ILanguageConfiguration,
	IFoldConfiguration,
} from './../config'
import { RegionTagType } from './Enums'
import { RegionsObject } from './Misc'
import * as Consts from './../Constants'
// import { Log } from './../utils'
import { EOL } from 'os'

//
// endregion Imports
//

//
// region RegionTag
//

export class RegionTag {
	public regionTagType: RegionTagType
	public startCharacter?: number
	public endCharacter?: number
	public fullText?: string
	public lineNumber: number
	public name?: string

	constructor(type: RegionTagType) {
		this.regionTagType = type
		this.lineNumber = -1
	}

	static Unknown() {
		return new RegionTag(RegionTagType.Unknown)
	}

	static FromRegex(regExpMatch: RegExpExecArray, tagType: RegionTagType, lineNumber: number) {
		const name = regExpMatch.length > 1 ? regExpMatch[1] : Consts.EMPTY_STR
		const regionTag = new RegionTag(tagType)

		regionTag.startCharacter = regExpMatch.index
		regionTag.endCharacter = regExpMatch[0].length
		regionTag.name = name
		regionTag.lineNumber = lineNumber
		regionTag.fullText = regExpMatch[0]

		return regionTag
	}
}

//
// endregion RegionTag
//

//
// region CustomRegion
//

export class CustomRegion {
	public startRegionTag: RegionTag
	public endRegionTag: RegionTag
	public isDefaultRegion = false

	constructor(startRegionTag: RegionTag, endRegionTag: RegionTag = RegionTag.Unknown()) {
		this.startRegionTag = startRegionTag
		this.endRegionTag = endRegionTag
	}

	public contains(position: Position) {
		const line = position.line
		return line >= this.lineStart && line <= this.lineEnd
	}

	public get lineStart(): number {
		if (this.startRegionTag) {
			return this.startRegionTag.lineNumber
		}

		return -1
	}

	public get lineEnd(): number {
		if (this.endRegionTag) {
			return this.endRegionTag.lineNumber
		}

		return -1
	}

	public get name(): string {
		if (this.startRegionTag && this.startRegionTag.name) {
			return this.startRegionTag.name
		}

		return Consts.EMPTY_STR
	}
}

//
// endregion CustomRegion
//

//
// region RegionProvider
//

export class RegionProvider {
	private mConfigurationService: ConfigurationService

	constructor(configService: ConfigurationService) {
		this.mConfigurationService = configService
	}

	public get configuration() {
		return this.mConfigurationService
	}

	public set configuration(value: ConfigurationService) {
		if (this.mConfigurationService !== value) {
			this.mConfigurationService = value
		}
	}

	public getRegions(document: TextDocument): RegionsObject {
		const languageId = document.languageId

		const currentLanguageConfig = this.mConfigurationService.getConfigurationForLanguage(languageId)

		if (!currentLanguageConfig) {
			return {
				completedRegions: [],
				errors: []
			}
		}

		const foldDefinitions = <IFoldConfiguration[]>[currentLanguageConfig]

		if (currentLanguageConfig.foldDefinitions) {
			for (const foldDefinition of currentLanguageConfig.foldDefinitions) {
				foldDefinitions.push(foldDefinition)
			}
		}

		const completedRegions: Array<CustomRegion> = []
		const text = document.getText()
		let lines = text.split(EOL)

		// lines = lines.map((item, _idx): any => {
		// 	if (item !== Consts.EMPTY_STR) {
		// 		return item
		// 	} else if (item !== null) {
		// 		return item
		// 	}
		// })

		lines = lines.filter((item) => {
			// Log.d('Line:', item)
			return (item !== null || item !== Consts.EMPTY_STR || item !== EOL)
		})

		// Log.d('Lines:', lines)

		const errors = []

		for (const foldDefinition of foldDefinitions) {
			const startedRegions: Array<CustomRegion> = []
			// @see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp
			const foldStartRE = new RegExp(foldDefinition.foldStartRegex, 'gi')
			const foldEndRE = new RegExp(foldDefinition.foldEndRegex, 'gi')

			for (let lineIndex = 0; lineIndex < lines.length; ++lineIndex) {
				const line = lines[lineIndex]
				const startMatch = foldStartRE.exec(line)
				const endMatch = foldEndRE.exec(line)

				if (startMatch) {
					const startRegionTag = RegionTag.FromRegex(startMatch, RegionTagType.Start, lineIndex)
					const customRegion = new CustomRegion(startRegionTag)

					if ((<ILanguageConfiguration>foldDefinition).defaultFoldStartRegex) {
						const regexPatt = <string>(<ILanguageConfiguration>foldDefinition).defaultFoldStartRegex
						const defaultRE = new RegExp(regexPatt, 'i')

						if (defaultRE.exec(line)) {
							customRegion.isDefaultRegion = true
						}
					}

					else if (foldDefinition.isFoldedByDefault) {
						customRegion.isDefaultRegion = true
					}

					startedRegions.push(customRegion)
				} else if (endMatch) {
					if (startedRegions.length === 0) {
						errors.push(
							`Found an end region with no matching start tag at line ${lineIndex}`
						)
						continue
					}

					const endTag = RegionTag.FromRegex(endMatch, RegionTagType.End, lineIndex)
					const lastStartedRegion = startedRegions[startedRegions.length - 1]
					const finishedRegion = new CustomRegion(lastStartedRegion.startRegionTag, endTag)

					finishedRegion.isDefaultRegion = lastStartedRegion.isDefaultRegion
					completedRegions.push(finishedRegion)
					startedRegions.pop()
				}
			}

			if (startedRegions.length > 0) {
				for (const err of startedRegions) {
					errors.push(
						`Found a started region with no matching end tag at line ${err.lineStart
						}`
					)
				}
			}
		}

		return {
			completedRegions: completedRegions,
			errors: errors
		}
	}
}

//
// endregion RegionProvider
//
