//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: Engine.ts
//

//
// region Imports
//

'use strict'

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {
	Position,
	TextDocument,
	Selection,
	Range,
	TextEditor,
	TextEditorRevealType,
	window,
	commands,
	languages,
} from 'vscode'
import { ConfigurationService } from './../config'
import {
	RegionWrapperService,
	RegionService,
	FileMonitor,
} from './'
import * as Consts from './../Constants'
import { Log } from './../utils'
import * as Exen from './../provider/FoldingRangeProvider'
import { RegionTreeItem } from './../views'

//
// endregion Imports
//

//
// region Engine
//

/**
 * @class Engine
 *
 * Folding Engine.
 *
 * @since 0.1.0
 */
export class Engine {
	/**
	 * Folding Range Provider
	 *
	 * @private
	 * @type {(Exen.FoldingRangeProvider | null)}
	 * @memberof Engine
	 */
	private mFoldingRangeProvider: Exen.FoldingRangeProvider | null = null
	private mConfigService: ConfigurationService
	private mFileMonitor: FileMonitor

	// private logger: Logging = Logging.getInstance()

	/**
	 *
	 */
	constructor(configService: ConfigurationService) {
		const self = this

		this.mConfigService = configService
		this.mFoldingRangeProvider = this.registerFoldingRangeProvider()

		this.mFileMonitor = new FileMonitor()

		this.mFileMonitor.onFileOpened.add(function (doc) {
			Log.d('File opened: ' + doc.fileName + ' lid: ' + doc.languageId)
			// Logging.getInstance().d('File opened: ' + doc.fileName + ' lid: ' + doc.languageId)

			if (doc.languageId) {
				// HMM HACK! No texteditor defined for document when this event has been called.
				const collapseOnlyDefaults = true
				setTimeout(() => { self.collapseAllRegions(doc, collapseOnlyDefaults) }, 10)

			}
		})

		this.mFileMonitor.onFileClosing.add(function (doc) {
			Log.d('File closing: ' + doc.fileName)
		})

		this.mFileMonitor.onLanguageIdChanged.add(function (doc, oldLID, newLID) {
			// window.showTextDocument(doc, 1, false).then(e => {
			//     e.edit(edit => {
			//         edit.insert(new Position(0, 0), 'Your advertisement here')
			//     })

			Log.d('FileMonitor has detected change in language: ' + newLID)

			if (newLID) {
				self.collapseAllRegions(doc)
			}
		})

		for (const vte of window.visibleTextEditors) {
			this.mFileMonitor.manuallyRegisterDocument(vte.document)
		}
	}

	public get FoldingRangeProvider() {
		return this.mFoldingRangeProvider
	}

	public dispose() {
		this.mFoldingRangeProvider = null
	}

	public selectCurrentRegion() {
		const activeTextEditor = window.activeTextEditor

		if (!activeTextEditor) { return }
		const document = activeTextEditor.document

		if (!document) { return }

		const rs = new RegionService(this.mConfigService, document)

		const currentRegion = rs.currentRegion()

		if (!currentRegion) { return }

		const srt = currentRegion.startRegionTag
		const start1 = new Position(srt.lineNumber, <number>srt.startCharacter)

		const ert = currentRegion.endRegionTag
		const endLine = activeTextEditor.document.lineAt(ert.lineNumber)

		if (!endLine) { return }

		const end2 = new Position(ert.lineNumber, endLine.text.length)

		activeTextEditor.selection = new Selection(start1, end2)
	}

	public selectCurrentRegionContents() {
		const activeTextEditor = window.activeTextEditor

		if (!activeTextEditor) { return }

		const document = activeTextEditor.document

		if (!document) { return }

		const rs = new RegionService(this.mConfigService, document)
		const currentRegion = rs.currentRegion()

		if (!currentRegion) { return }

		const srt = currentRegion.startRegionTag
		const startLineNumber = srt.lineNumber + 1
		const endLineNumber = currentRegion.endRegionTag.lineNumber - 1

		if (endLineNumber < startLineNumber) { return }

		const startLine = activeTextEditor.document.lineAt(startLineNumber)
		const endLine = activeTextEditor.document.lineAt(endLineNumber)

		const start1 = startLine.range.start
		const end1 = endLine.range.end

		activeTextEditor.selection = new Selection(start1, end1)
	}

	public editCurrentRegion(node: RegionTreeItem) {
		window.showInformationMessage(`Successfully called edit entry on ${node.label}.`)
	}

	public removeCurrentRegionTags() {
		window.showInformationMessage('Remove current region tags')

		const activeTextEditor = window.activeTextEditor

		if (!activeTextEditor) { return }

		const document = activeTextEditor.document

		if (!document) { return }

		const rs = new RegionService(this.mConfigService, document)
		const currentRegion = rs.currentRegion()

		if (!currentRegion) { return }

		activeTextEditor.edit(edit => {
			if (!currentRegion) { return }
			if (!activeTextEditor) { return }

			const srt = currentRegion.startRegionTag
			const start1 = new Position(srt.lineNumber, <number>srt.startCharacter)
			const startLine = activeTextEditor.document.lineAt(srt.lineNumber)
			const end1 = new Position(srt.lineNumber, startLine.text.length)
			let range = new Range(start1, end1)

			edit.delete(range)

			const ert = currentRegion.endRegionTag
			const endLine = activeTextEditor.document.lineAt(ert.lineNumber)

			if (!endLine) { return }

			const start2 = new Position(ert.lineNumber, <number>ert.startCharacter)
			const end2 = new Position(ert.lineNumber, endLine.text.length)

			range = new Range(start2, end2)

			edit.delete(range)

			// tslint:disable-next-line:no-unused-expression
		})
	}

	public deleteCurrentRegion() {
		window.showInformationMessage('Delete current region tags')

		const activeTextEditor = window.activeTextEditor

		if (!activeTextEditor) { return }

		const document = activeTextEditor.document

		if (!document) { return }

		const rs = new RegionService(this.mConfigService, document)
		const currentRegion = rs.currentRegion()

		if (!currentRegion) { return }

		activeTextEditor.edit(edit => {
			if (!currentRegion) { return }
			if (!activeTextEditor) { return }

			const srt = currentRegion.startRegionTag
			const start = new Position(srt.lineNumber, <number>srt.startCharacter)
			const ert = currentRegion.endRegionTag

			const endLine = activeTextEditor.document.lineAt(ert.lineNumber)

			if (!endLine) { return }

			const endLineT = endLine.text

			const end = new Position(ert.lineNumber, endLineT.length)

			const range = new Range(start, end)

			edit.delete(range)

			// tslint:disable-next-line:no-unused-expression
		})
	}

	public collapseAllRegions(document: TextDocument | null = null, onlyDefaults = false) {
		// window.showInformationMessage('collapse all regions')

		if (!document) {
			const activeTextEditor = window.activeTextEditor

			if (!activeTextEditor) { return }

			document = activeTextEditor.document

			if (!document) { return }
		}

		Log.d('Collapsing all regions')

		//
		// region New Code
		//

		const rs = new RegionService(this.mConfigService, document)
		const regions = rs.getRegions()
		const arr = []

		for (const region of regions) {
			if (onlyDefaults && !region.isDefaultRegion) {
				continue
			}

			arr.push(region.lineStart)
		}

		//
		// endregion New Code
		//

		//
		// region Old Code
		//

		// let currentLanguageConfig = this.mConfigService.getConfigurationForCurrentLanguage(document.languageId)
		// if (!currentLanguageConfig) { return }
		// if (!currentLanguageConfig.foldStartRegex) { return }
		//
		// let arr = []
		//
		// for (let x = 0 x < document.lineCount ++x) {
		//     let line = document.lineAt(x)
		//     let start = new RegExp(currentLanguageConfig.foldStartRegex, 'i')
		//     if (start.exec(line.text.toString())) {
		//         arr.push(x)
		//     }
		// }

		//
		// endregion Old Code
		//

		this.foldLines(document, arr)
	}

	public collapseAllDefaultFolds(document: TextDocument | null = null) {
		this.collapseAllRegions(document, true)
	}

	public revealRegion(line: number) {
		const editor = window.activeTextEditor

		if (editor == undefined) {
			return
		}

		const position = new Position(line, 0)

		editor.selection = new Selection(position, position)
		editor.revealRange(editor.selection, TextEditorRevealType.InCenter)
	}

	private getTextEditor(document: TextDocument): TextEditor | null {
		for (const te of window.visibleTextEditors) {
			if (te.document.fileName === document.fileName) {
				return te
			}
		}

		return null
	}

	private async foldLines(document: TextDocument, foldLines: Array<number>) {
		let str = Consts.EMPTY_STR

		foldLines.forEach(p => str += p + ',')

		Log.d('Folding lines: ' + str)

		const textEditor = this.getTextEditor(document)

		if (!textEditor) { return }

		const selection = textEditor.selection

		for (const lineNumber of foldLines) {
			textEditor.selection = new Selection(lineNumber, 0, lineNumber, 0)
			await commands.executeCommand('editor.fold')
			Log.d('Folding ' + textEditor.selection.anchor.line)
		}

		textEditor.selection = selection

		// textEditor.revealRange(textEditor.selection, TextEditorRevealType.InCenter)
	}

	public wrapWithRegionAndComment() {
		commands.executeCommand(
			'editor.action.commentLine',
			'editorHasDocumentFormattingProvider && editorTextFocus',
			true
		).then(() => {
			const textEditor = window.activeTextEditor

			if (!textEditor) { return }

			const selection = textEditor.selection

			const newStart = new Position(selection.start.line, 0)
			const newEnd = textEditor.document.lineAt(selection.end.line).range.end

			textEditor.selection = new Selection(newStart, newEnd)

			commands.executeCommand(
				`${Consts.SIMPLE_NAME}.wrapWithRegion`,
				'editorHasDocumentFormattingProvider && editorTextFocus',
				true
			)
		})
	}

	public wrapWithRegion() {
		const regionWrapper = new RegionWrapperService(this.mConfigService)
		regionWrapper.wrapCurrentWithRegion()
	}

	private registerFoldingRangeProvider() {
		const supportedLanguages = this.mConfigService.getSupportedLanguages()
		const foldingRangeProvider = new Exen.FoldingRangeProvider(this.mConfigService)

		languages.registerFoldingRangeProvider(
			supportedLanguages,
			foldingRangeProvider
		)

		this.mConfigService.onConfigurationChanged = () => {
			foldingRangeProvider.configurationService = this.mConfigService
		}

		return foldingRangeProvider
	}
}

//
// endregion Engine
//
