//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Imports
//

'use strict'

//
// endregion Imports
//

//
// region Enums
//

/**
 * @enum RegionTagType
 *
 * Region Tag Type Enum.
 *
 * @since 0.1.0
 */
export enum RegionTagType {
	Unknown    = 0x0,
	Start      = 0x1,
	End        = 0x2,
}

/**
 * @enum EndOfLine
 *
 * Represents an end of line character sequence.
 *
 * @since 0.1.0
 */
export enum EndOfLine {
	/**
	 * The line feed `\n` character.
	 */
	LF   = 0x1,

	/**
	 * The carriage return line feed `\r\n` sequence.
	 */
	CRLF = 0x2
}

//
// endregion Enums
//
