//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: FileMonitor.ts
//

//
// region Imports
//

'use strict'

import {
	TextDocument,
	workspace,
	// TextEditor,
	// window
} from 'vscode'

import { Log } from './../utils'

//
// endregion Imports
//

//
// region DocumentRecord
//

class DocumentRecord {
	public interval: NodeJS.Timer | null = null
	public document: TextDocument
	public languageId: string
	public trackStartTime: Date = new Date()

	/**
	 *
	 */
	constructor(document: TextDocument, languageId: string) {
		this.document = document
		this.languageId = languageId

	}
}

//
// endregion DocumentRecord
//

//
// region EventHandler
//

class EventHandler<T extends (...args: any[]) => void>{
	private subscriptions: T[] = []

	public add(t: T) {
		this.subscriptions.push(t)
	}

	public invoke(...args: any[]) {
		// const ams = <any[]>[].slice.call(arguments)
		const ams = <any[]>[].slice.call(args)

		ams.unshift({})

		for (const sub of this.subscriptions) {
			sub.apply({}, args)
		}
	}

	public remove(t: T) {
		let idx = -1

		for (let x = 0; x < this.subscriptions.length; ++x) {
			const sub = this.subscriptions[x]

			if (sub === t) {
				idx = x
				break
			}
		}

		if (idx === -1) { return }

		this.subscriptions.splice(idx, 1)
	}
}

//
// endregion EventHandler
//

//
// region FileMonitorSettings
//

export class FileMonitorSettings {
	stopTrackingAfterChange = true
	stopTrackingAfterMs: number | null = 2000
	trackLanguageIdChanges = true
}

//
// endregion FileMonitorSettings
//

//
// region FileMonitor
//

export class FileMonitor {
	public onLanguageIdChanged: EventHandler<((document: TextDocument, oldLanguageId: string, newLanguageId: string) => void)>
	public onFileOpened: EventHandler<((document: TextDocument) => void)>
	public onFileClosing: EventHandler<((document: TextDocument) => void)>
	private mDocumentDictionary: DocumentRecord[] = []
	mSettings: FileMonitorSettings

	constructor(settings: FileMonitorSettings | null = null) {
		// context: ExtensionContext,

		const defaultFileMonitorSettings = new FileMonitorSettings()

		this.mSettings = defaultFileMonitorSettings
		this.onLanguageIdChanged = new EventHandler<((document: TextDocument, oldLanguageId: string, newLanguageId: string) => void)>()
		this.onFileOpened = new EventHandler<((document: TextDocument) => void)>()
		this.onFileClosing = new EventHandler<((document: TextDocument) => void)>()

		// TODO: Fix me
		// workspace.onDidChangeTextDocument((doc) => {
		// 	const textEditor = this.getTextEditor(doc.document)
		// 	Log.d('Document: ' + textEditor?.document.fileName + ' TextEditor: ' + textEditor)
		// }, null)

		workspace.onDidOpenTextDocument((dpc) => {
			Log.d('Document: ' + dpc.getText)
			this.fileOpening(dpc)
		}, null)

		workspace.onDidCloseTextDocument((dpc) => {
			Log.d('Document: ' + dpc.getText)
			this.fileClosing(dpc)
		}, this)
	}

	// TODO: Fix me
	// private getTextEditor(document: TextDocument): TextEditor | null {
	// 	for (const visibleTextEditor of window.visibleTextEditors) {
	// 		if (visibleTextEditor.document.fileName === document.fileName) {
	// 			return visibleTextEditor
	// 		}
	// 	}

	// 	return null
	// }

	public manuallyRegisterDocument(doc: TextDocument) {
		this.fileOpening(doc)
	}

	private findDocumentRecord(document: TextDocument) {
		let selectedIndex = -1

		for (let x = 0; x < this.mDocumentDictionary.length; ++x) {
			const docRecordI = this.mDocumentDictionary[x]

			if (docRecordI.document === document) {
				selectedIndex = x
				break
			}
		}

		if (selectedIndex === -1) { return { record: null, index: selectedIndex } }

		const docRecord = this.mDocumentDictionary[selectedIndex]

		return { record: docRecord, index: selectedIndex }
	}

	private stopTrackingDocument(document: TextDocument) {
		Log.d('Stop tracking doc ' + document.fileName)

		const ret = this.findDocumentRecord(document)

		if (!ret.record) { return }

		this.mDocumentDictionary.splice(ret.index, 1)

		const docRecord = ret.record

		if (docRecord.interval) { clearInterval(docRecord.interval) }

		docRecord.interval = null
	}

	protected fileClosing(document: TextDocument) {
		this.onFileClosing.invoke(document)
		this.stopTrackingDocument(document)
	}

	protected fileOpening(document: TextDocument) {
		this.onFileOpened.invoke(document)
		this.onLanguageIdChanged.invoke(document, null, document.languageId)

		if (!this.mSettings.trackLanguageIdChanges) { return }

		const documentRecord = new DocumentRecord(document, document.languageId)

		documentRecord.trackStartTime = new Date()

		this.mDocumentDictionary.push(documentRecord)

		documentRecord.interval = setTimeout(() => {
			Log.d('DocumentRecord Interval running for doc ' + document.fileName)

			const ret = this.findDocumentRecord(document)

			if (!ret.record) { return }

			if (ret.record.languageId !== document.languageId) {
				// The LanguageID has changed.
				this.raiseLanguageIdChanged(ret.record.document, ret.record.languageId, document.languageId)
				ret.record.languageId = document.languageId

				if (this.mSettings.stopTrackingAfterChange) {
					this.stopTrackingDocument(document)
				}
			}

			const now = new Date().getTime()
			const elapsedTimeMs = now - ret.record.trackStartTime.getTime()

			if (this.mSettings.stopTrackingAfterMs) {
				if (elapsedTimeMs >= this.mSettings.stopTrackingAfterMs) {
					this.stopTrackingDocument(document)
				}
			}
		}, 10)
	}

	private raiseLanguageIdChanged(document: TextDocument, oldLanguageId: string, newLanguageId: string) {
		this.onLanguageIdChanged.invoke(document, oldLanguageId, newLanguageId)
	}
}

//
// endregion FileMonitor
//
