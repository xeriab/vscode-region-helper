//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Imports
//

'use strict'

import { FoldingRange } from 'vscode'
import { CustomRegion } from './CustomRegions'

//
// endregion Imports
//

//
// region Misc.
//

/**
 * @type {FoldingRangeArray}
 *
 * Folding Range Array Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type FoldingRangeArray = Array<FoldingRange>

/**
 * @type {CustomRegionArray}
 *
 * Region Helper Array Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type CustomRegionArray = Array<CustomRegion>

/**
 * @type {CustomRegionType}
 *
 * Region Helper Or Null Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type CustomRegionType = CustomRegion|null

/**
 * @type {RegionsIntArrayObject}
 *
 * Integer Array Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type IntArray = Int32Array

/**
 * @type {NumberArray}
 *
 * Number Array Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type NumberArray = Array<number>

/**
 * @type {StringArray}
 *
 * String Array Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type StringArray = Array<string>

/**
 * @interface IRegionsObject
 *
 * Regions Object Interface.
 *
 * @since 0.1.0
 *
 * @api
 */
export interface IRegionsObject {
	completedRegions: Array<CustomRegion>,
	errors: Array<string>
}

/**
 * @type {RegionsObject}
 *
 * Regions Object Type.
 *
 * @since 0.1.0
 *
 * @api
 */
export type RegionsObject = IRegionsObject

//
// endregion Misc.
//
