//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: RegionServices.ts
//

'use strict'

//
// region Imports
//

import { TextDocument, window } from 'vscode'
import { ConfigurationService } from './../config'
import { RegionProvider, CustomRegion } from './CustomRegions'

//
// endregion Imports
//

//
// region RegionService
//

export class RegionService {
	regionProvider: RegionProvider
	document: TextDocument
	regions: CustomRegion[]

	/**
	 *
	 */
	constructor(configService: ConfigurationService, document: TextDocument) {
		this.regionProvider = new RegionProvider(configService)
		this.document = document
		this.regions = []
	}

	public update() {
		const result = this.regionProvider.getRegions(this.document)
		this.regions = result.completedRegions
	}

	public getRegions() {
		this.update()

		return this.regions
	}

	public currentRegions(): CustomRegion[] {
		this.update()

		const activeTextEditor = window.activeTextEditor

		if (!activeTextEditor) { return [] }

		if (this.document !== activeTextEditor.document) {
			return []
		}

		const surroundingRegions = []

		for (const reg of this.regions) {
			if (reg.contains(activeTextEditor.selection.active)) {
				surroundingRegions.push(reg)
			}
		}

		return surroundingRegions
	}

	public currentRegion(): CustomRegion | null {
		const currentRegions = this.currentRegions()

		if (currentRegions.length === 0) { return null }

		// return currentRegions[0]
		return currentRegions[currentRegions.length - 1]
	}
}

//
// endregion RegionService
//

