//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: RegionWrapper.ts
//

'use strict'

//
// region Imports
//

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {
	// EndOfLine,
	Selection,
	Position,
	Range,
	window,
	commands
} from 'vscode'
import { ConfigurationService } from './../config'
import { EndOfLine } from './Enums'
import * as Consts from './../Constants'
// import { Log } from './../utils'

//
// endregion Imports
//

//
// region RegionWrapperService
//

export class RegionWrapperService {
	mConfigService: ConfigurationService

	/**
	 *
	 */
	constructor(configService: ConfigurationService) {
		this.mConfigService = configService
	}

	public wrapCurrentWithRegion() {
		const activeTextEditor = window.activeTextEditor

		if (!activeTextEditor) { return }

		const document = activeTextEditor.document

		if (!document) { return }

		const currentLanguageConfig = this.mConfigService.getConfigurationForCurrentLanguage(document.languageId)

		// Log.d(currentLanguageConfig)
		// Log.d(currentLanguageConfig?.foldEnd)
		// Log.d(currentLanguageConfig?.foldStart)

		if (!currentLanguageConfig) { return }

		//
		// region Check if there is anything selected.
		//

		if (activeTextEditor.selections.length > 1 || activeTextEditor.selections.length < 1) {
			return
		}

		const sel = activeTextEditor.selection

		if (sel.isEmpty) {
			return
		}

		//
		// endregion Check if there is anything selected.
		//

		const linePrefix = activeTextEditor.document.getText(
			new Range(new Position(sel.start.line, 0), sel.start)
		)

		let addPrefix = Consts.EMPTY_STR

		if (/^\s+$/.test(linePrefix)) {
			addPrefix = linePrefix
		}

		const eol = this.getEolStr(activeTextEditor.document.eol)

		// Get the position of [NAME] in the fold start template.
		const regionStartTemplate = currentLanguageConfig.foldStart

		const idx = regionStartTemplate.indexOf('[NAME]')

		const nameInsertionIndex =
			idx < 0 ? 0 : regionStartTemplate.length - '[NAME]'.length - idx

		const regionStartText = regionStartTemplate.replace('[NAME]', Consts.EMPTY_STR)

		activeTextEditor
			.edit(edit => {
				if (!currentLanguageConfig) { return }
				if (!activeTextEditor) { return }

				// Insert the #region, #endregion tags
				edit.insert(
					sel.end,
					eol + addPrefix + currentLanguageConfig.foldEnd
				)

				edit.insert(sel.start, regionStartText + eol + addPrefix)
			})
			.then(_edit => {
				if (!currentLanguageConfig) { return }
				if (!activeTextEditor) { return }

				// Now, move the selection point to the [NAME] position.
				const sel = activeTextEditor.selection
				const newLine = sel.start.line - 1
				const newChar =
					activeTextEditor.document.lineAt(newLine).text.length - nameInsertionIndex
				const newStart = sel.start.translate(
					newLine - sel.start.line,
					newChar - sel.start.character
				)
				const newSelection = new Selection(newStart, newStart)

				activeTextEditor.selections = [newSelection]

				// Format the document
				commands.executeCommand(
					'editor.action.formatDocument',
					'editorHasDocumentFormattingProvider && editorTextFocus',
					true
				)
			})
	}

	private getEolStr(eol: EndOfLine): string {
		if (eol === EndOfLine.CRLF) {
			return Consts.EOL_CRLF
		}

		return Consts.EOL
	}
}

//
// endregion RegionWrapperService
//
