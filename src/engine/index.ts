//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Exports
//

'use strict'

export {
	RegionTag,
	CustomRegion,
	RegionProvider
} from './CustomRegions'

export {
	Engine
} from './Engine'

export {
	FileMonitorSettings,
	FileMonitor
} from './FileMonitor'

export {
	RegionService
} from './RegionServices'

export {
	RegionWrapperService
} from './RegionWrapper'

export {
	RegionTagType
} from './Enums'

export {
	CustomRegionArray
} from './Misc'

//
// endregion Exports
//
