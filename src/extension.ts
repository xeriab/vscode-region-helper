//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: extension.ts
//

//
// region Imports
//

'use strict'

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {
	ExtensionContext,
	commands,
	window,
	workspace,
	// TextEditor,
	// Disposable,
	// Position,
	// TextEditorRevealType,
	// Selection,
	// QuickPickItem,
} from 'vscode'
import { ConfigurationService, ICommandArray, IConfiguration } from './config'
import { Engine } from './engine'
import {
	Log,
	Logging,
} from './utils'
import { RegionTreeProvider } from './provider';
import * as Consts from './Constants'
// import * as path from 'path'
// import * as fs from 'fs'

//
// endregion Imports
//

// const self: any|null|undefined = this || global || window
// let currentEditor: TextEditor|null|undefined
// let currentContext: ExtensionContext|null|undefined
// let statusSizeDisposable: Disposable|null|undefined
// let instanceMap: Array<any> | null = null
export const logger: Logging = Logging.register(Consts.EXTENSION_NAME)
let extensionConfig: IConfiguration | null = null

//
// region Extension Methods
//

/**
 * Deactivate [`Visual Studio Code`](https://code.visualstudio.com/) or
 * [`VSCodium`](https://vscodium.com/) extension.
 *
 * NOTE: This method is called when the extension is deactivated.
 *
 * @return {Void} - Nothing
 *
 * @since 0.1.0
 */
export const deactivate = (): void => {
	logger.info('The extension is deactivated')
	Log.info('The extension is deactivated')
}

/**
 * Reactivate [`Visual Studio Code`](https://code.visualstudio.com/) or
 * [`VSCodium`](https://vscodium.com/) extension.
 *
 * NOTE: This method is called when the extension is reactivated.
 *
 * @return {Void} - Nothing
 *
 * @since 0.1.0
 */
export const reactivate = (): void => {
	deactivate()

	extensionConfig = null

	logger.info('The extension is reactivated')
	Log.info('The extension is reactivated')
}

/**
 * On Configuration change event.
 *
 * @returns {Void} Nothing
 *
 * @since 0.1.0
 */
const onConfigurationChange = () => {
	extensionConfig = workspace.getConfiguration()
		.get(Consts.EXTENSION_NAMESPACE) as IConfiguration

	// reactivate()
}

export const activateTreeView = (context: ExtensionContext): void => {
	window.registerTreeDataProvider(
		'regionViewer',
		new RegionTreeProvider(context)
	)
}

/**
 * Activate [`Visual Studio Code`](https://code.visualstudio.com/) or
 * [`VSCodium`](https://vscodium.com/) extension.
 *
 * NOTE: This method is called when the extension is activated.
 *
 * @param {ExtensionContext} context [`Visual Studio Code`](https://code.visualstudio.com/) or [`VSCodium`](https://vscodium.com/) extension context.
 *
 * @return {Void} - Nothing
 *
 * @since 0.1.0
 */
export const activate = (context: ExtensionContext): void => {
	//
	// region Initial Activation
	//

	// instanceMap = []

	// currentContext = context
	// currentEditor = window.activeTextEditor

	// logger.info(currentContext)
	// logger.info(currentEditor)
	// logger.info(self)

	// Log.d(currentContext)
	// Log.d(currentEditor)
	// Log.d(self)

	const configService = new ConfigurationService(context)
	const engine = new Engine(configService)
	const regionTreeDataProvider = new RegionTreeProvider(context)

	// const extLanguages = workspace.getConfiguration()
	// 	.get('region-helper.languages') || {}

	// const enableRegionViewer = workspace.getConfiguration()
	// 	.get('region-helper.enableRegionViewer') || false

	// const enableColoring = workspace.getConfiguration()
	// 	.get('region-helper.enableColoring') || false

	extensionConfig = workspace.getConfiguration()
		.get(Consts.EXTENSION_NAMESPACE) as IConfiguration

	// Log.d('Configuration:', extensionConfig)

	const editorCommands: ICommandArray = []
	const extensionCommands: ICommandArray = []

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.wrapWithRegion`,
		action: () => engine.wrapWithRegion()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.wrapWithRegionAndComment`,
		action: () => engine.wrapWithRegionAndComment()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.collapseDefault`,
		action: () => engine.collapseAllDefaultFolds()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.collapseAllRegions`,
		action: () => engine.collapseAllRegions()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.deleteRegion`,
		action: () => engine.deleteCurrentRegion()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.removeCurrentRegionTags`,
		action: () => engine.removeCurrentRegionTags()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.selectCurrentRegion`,
		action: () => engine.selectCurrentRegion()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.selectCurrentRegionContents`,
		action: () => engine.selectCurrentRegionContents()
	})

	editorCommands.push({
		name: `${Consts.SIMPLE_NAME}.editRegion`,
		action: () => engine.editCurrentRegion
	})

	if (extensionConfig.enableRegionViewer) {
		// If a TreeView item is selected, the cursor moves to that location.
		extensionCommands.push({
			name: `${Consts.SIMPLE_NAME}.revealRegion`,
			description: `Reveal or goto region.`,
			action: (line: number) => engine.revealRegion(line)
		})
	}

	// logger.info('The extension is now active!')
	Log.info('The extension is now active!')

	for (const command of editorCommands) {
		// The command has been defined in the package.json file
		// Now provide the implementation of the command with registerCommand
		// The commandId parameter must match the command field in package.json
		const disposable = commands.registerTextEditorCommand(
			command.name,
			command.action
		)

		context.subscriptions.push(disposable)
	}

	for (const extensionCommand of extensionCommands) {
		// The command has been defined in the package.json file
		// Now provide the implementation of the command with registerCommand
		// The commandId parameter must match the command field in package.json
		const disposable = commands.registerCommand(
			extensionCommand.name,
			extensionCommand.action
		)

		context.subscriptions.push(disposable)
	}

	if (extensionConfig.enableRegionViewer) {
		context.subscriptions.push(
			window.registerTreeDataProvider(
				'regionViewer',
				regionTreeDataProvider
			),
			window.registerTreeDataProvider(
				'regionExplorer',
				regionTreeDataProvider
			)
		)

		// window.createTreeView('regionViewer', {
		// 	treeDataProvider: regionTreeDataProvider
		// })

		// When the document is changed, the list of regions in the document is
		// checked and updated.
		context.subscriptions.push(window.onDidChangeTextEditorSelection(() => {
			regionTreeDataProvider.refresh()
		}))

		// If a TreeView item is selected, the cursor moves to that location.
		editorCommands.push({
			name: `${Consts.SIMPLE_NAME}.revealRegion`,
			description: `Reveal region in current document.`,
			action: (line: number) => engine.revealRegion(line)
		})
	}

	// const onDidOpenTextDocument = workspace.onDidOpenTextDocument(() => {
	// 	commands.executeCommand('editor.foldAllMarkerRegions')
	// })

	// if (extensionConfig.autoFoldAllRegions) {
	// 	context.subscriptions.push(onDidOpenTextDocument)
	// }

	// TODO: Fix me
	// if (extensionConfig.enableRegionViewer) {
	// 	context.subscriptions.push(
	// 		window.registerTreeDataProvider(
	// 			'regionViewer',
	// 			regionTreeDataProvider
	// 		)
	// 	)

	// 	// window.createTreeView('regionViewer', {
	// 	// 	treeDataProvider: regionTreeDataProvider
	// 	// })

	// 	// When the document is changed, the list of regions in the document is
	// 	// checked and updated.
	// 	context.subscriptions.push(window.onDidChangeTextEditorSelection(() => {
	// 		regionTreeDataProvider.refresh()
	// 	}))
	// }

	workspace.onDidChangeConfiguration(
		onConfigurationChange,
		null,
		context.subscriptions
	)

	//
	// endregion Initial Activation
	//
}

//
// endregion Extension Methods
//
