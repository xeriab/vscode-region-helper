//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Exports
//

'use strict'

export {
	EMPTY_STR,
	EOL_CRLF,
	EOL_LF,
	EOL,
	EXTENSION_NAME,
	EXTENSION_NAMESPACE,
	FALSE,
	NULL_CHAR,
	NULL,
	SIMPLE_NAME,
	TRUE
} from './Constants'

export {
	activate,
	deactivate,
	reactivate,
	logger,
} from './extension'

//
// endregion Exports
//
