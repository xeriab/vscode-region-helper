//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: FoldingRangeProvider.ts
//

//
// region Imports
//

'use strict'

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {
	FoldingRangeProvider as IFoldingRangeProvider,
	TextDocument,
	FoldingContext,
	CancellationToken,
	FoldingRange,
	FoldingRangeKind,
	ProviderResult,
} from 'vscode'
import { ConfigurationService } from './../config'
import { FoldingRangeArray } from './../engine/Misc'
import * as Regions from './../engine/CustomRegions'

//
// endregion Imports
//

//
// region FoldingRangeProvider
//

export class FoldingRangeProvider implements IFoldingRangeProvider {
	private mConfigurationService: ConfigurationService

	constructor(config: ConfigurationService) {
		this.mConfigurationService = config
		// You may have to implement a default Folding range provider:
		// @see: https://github.com/Microsoft/vscode/blob/master/src/vs/editor/contrib/folding/indentRangeProvider.ts
	}

	public get configurationService() {
		return this.mConfigurationService
	}

	public set configurationService(value: ConfigurationService) {
		if (this.mConfigurationService !== value) {
			this.mConfigurationService = value
		}
	}

	provideFoldingRanges(
		document: TextDocument,
		context: FoldingContext,
		token: CancellationToken
	): ProviderResult<FoldingRangeArray> {
		const regionProvider = new Regions.RegionProvider(this.configurationService)
		const regions = regionProvider.getRegions(document)

		const retVal: FoldingRangeArray = []

		for (const region of regions.completedRegions) {
			const foldingRange = new FoldingRange(
				region.lineStart,
				<number>region.lineEnd,
				FoldingRangeKind.Region
			)

			retVal.push(foldingRange)
		}

		return retVal
	}
}

//
// endregion FoldingRangeProvider
//
