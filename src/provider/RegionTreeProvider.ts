//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: RegionTreeProvider.ts
//

//
// region Imports
//

'use strict'

import {
	commands,
	DragAndDropController,
	// Event,
	EventEmitter,
	ExtensionContext,
	// ProviderResult,
	Range,
	TextDocumentChangeEvent,
	TextEditor,
	TreeDataProvider,
	TreeItem,
	TreeItemCollapsibleState,
	window,
	workspace,
	// WorkspaceFolder,
} from 'vscode'
import * as Consts from '../Constants'
import * as Markers from '../Markers.json'
import {
	IConfiguration,
} from '../config'
import {
	RegionTreeItem,
	RegionTreeItemType,
	RegionTreeItemArray,
	RegionTreeItemEventEmitter,
	RegionTreeItemEvent,
} from '../views'
import {
	Log,
	// Logging,
} from '../utils'
import { logger } from '../'
// import * as json from 'jsonc-parser'
// import * as path from 'path'
// import * as fs from 'fs'

//
// endregion Imports
//

// export class RegionTreeProvider implements TreeDataProvider<RegionTreeItem> {
export class RegionTreeProvider implements TreeDataProvider<RegionTreeItem>, DragAndDropController<RegionTreeItem> {
	// private logger: Logging = Logging.getInstance()

	private mWorkspaceRoot!: string | undefined | null
	// private mWorkspaceFolders?: WorkspaceFolder[] | undefined

	private mData?: RegionTreeItemArray

	private _onDidChangeTreeData: RegionTreeItemEventEmitter = new EventEmitter<RegionTreeItemType>()
	readonly onDidChangeTreeData: RegionTreeItemEvent = this._onDidChangeTreeData.event

	private mText?: string
	private mEditor?: TextEditor
	private mAutoRefresh?: boolean = false
	private mContext?: ExtensionContext

	constructor(
		protected readonly extensionContext?: ExtensionContext | undefined,
		// protected readonly folders?: WorkspaceFolder[] | undefined,
		// protected readonly root?: string | undefined
	) {
		this.mContext = extensionContext
		// this.mWorkspaceFolders = folders
		// this.mWorkspaceRoot = root
		this.mWorkspaceRoot = workspace.workspaceFolders![0].uri.path ?? null
		// this.logger.d(workspace.workspaceFolders)
		// this.logger.d(this.mWorkspaceRoot)

		window.onDidChangeActiveTextEditor(() => this.onActiveEditorChanged())
		workspace.onDidChangeTextDocument(evt => this.onDocumentChanged(evt))

		let config: IConfiguration = workspace.getConfiguration()
			.get(Consts.EXTENSION_NAMESPACE) as IConfiguration

		this.autoRefresh = config['autoRefresh'] ?? false

		workspace.onDidChangeConfiguration(() => {
			config = workspace.getConfiguration()
				.get(Consts.EXTENSION_NAMESPACE) as IConfiguration
			this.autoRefresh = config['autoRefresh'] ?? false
		})

		this.onActiveEditorChanged()

		this.findRegions()
	}

	refresh(): void {
		this.findRegions()
		this._onDidChangeTreeData.fire()
	}

	getTreeItem(element: RegionTreeItem): TreeItem {
		return element
	}

	// getChildren(element?: RegionTreeItem): ProviderResult<RegionTreeItemArray> {
	getChildren(element?: RegionTreeItem): Thenable<RegionTreeItemArray> {
		if (!this.workspaceRoot) {
			window.showInformationMessage('No regions found in empty workspace')
			return Promise.resolve([])
		}

		if (element === undefined) {
			// return this.data
			return Promise.resolve(this.getRegions())
		}

		// return element.children
		return Promise.resolve(element.children)
	}

	private getRegions(): RegionTreeItemArray {
		const retVal: RegionTreeItemArray = []

		if (this.data !== undefined) {
			for (const item of this.data) {
				retVal.push(item)
			}
		}

		return retVal
	}

	/**
	 * Find regions.
	 *
	 * @returns {void}
	 *
	 * @memberof RegionTreeProvider
	 */
	private findRegions(): void {
		const document = window.activeTextEditor?.document

		if (document === undefined) {
			return
		}

		const treeRoot: RegionTreeItemArray = []
		const regionStack: RegionTreeItemArray = []

		if (document.languageId in Markers) {
			// Create Generally Typed Markers, so that we can
			// index them using languageID strings
			const RDMarkers: {
				[language: string]: {
					'foldStartRegex': string,
					'foldEndRegex': string
				}
			} = Markers

			const startRegExp = new RegExp(RDMarkers[document.languageId].foldStartRegex)
			const endRegExp = new RegExp(RDMarkers[document.languageId].foldEndRegex)

			const isRegionStart = (t: string) => startRegExp.test(t)
			const isRegionEnd = (t: string) => endRegExp.test(t)

			for (let lineNo = 0; lineNo < document.lineCount; ++lineNo) {
				const line = document.lineAt(lineNo)

				if (line === undefined) {
					continue
				}

				const range = new Range(line.range.start, line.range.end)
				const text = document.getText(range)

				if (text === undefined) {
					continue
				}

				if (isRegionStart(text)) {
					const regexResult = startRegExp.exec(text)
					const name = this.getName(text, regexResult)
					const region = new RegionTreeItem(
						name,
						false,
						TreeItemCollapsibleState.Expanded,
						// {
						// 	command: Consts.EMPTY_STR,
						// 	title: Consts.EMPTY_STR,
						// 	arguments: [],
						// },
						{
							title: `Reveal region in current document.`,
							command: `${Consts.SIMPLE_NAME}.revealRegion`,
							arguments: [lineNo],
						},
						lineNo
					)

					// If we have a parent, register as their child
					if (regionStack.length > 0) {
						const parentReg = regionStack[regionStack.length - 1]
						parentReg.children?.push(region)
						parentReg.collapsibleState = TreeItemCollapsibleState.Expanded
					}

					regionStack.push(region)
				} else if (isRegionEnd(text)) {
					// If we just ended a root region, add it to treeRoot
					if (regionStack.length === 1) {
						treeRoot.push(regionStack[0])
					}

					regionStack.pop()
				}
			}

			// If the region stack isn't empty, we didn't properly close all regions
			if (regionStack.length > 0) {
				treeRoot.push(regionStack[0])
			}

			this.data = treeRoot
		}
	}

	/**
	 * Get name.
	 *
	 * @param {String} input Input string.
	 * @param {(RegExpExecArray | null)} match Regex match.
	 *
	 * @returns {string}
	 *
	 * @memberof RegionTreeProvider
	 */
	private getName(input: string, match: RegExpExecArray | null): string {
		if (match && match.groups) {
			const groupIDs = [
				'name',
				'nameAlt'
			]

			// Look into capture groups
			for (const groupID of groupIDs) {
				if (groupID in match.groups && match.groups[groupID] !== undefined) {
					const name = match.groups[groupID].trim()

					if (name.length > 0) {
						// TODO: fix me
						// return '# ' + name
						return name.trim()
					}
				}
			}

			// Empty region name
			// TODO: fix me
			// return '# region'
			return 'region'
		} else {
			// Regex error or no groups found
			return input
		}
	}

	/**
	 * Gets workspace root.
	 *
	 * @readonly
	 * @type {(string | undefined | null)}
	 * @memberof RegionTreeProvider
	 */
	public get workspaceRoot(): string | undefined | null {
		return this.mWorkspaceRoot
	}

	/**
	 * Sets workspace root.
	 *
	 * @param {(string | undefined | null)} value Value.
	 *
	 * @memberof RegionTreeProvider
	 */
	public set workspaceRoot(value: string | undefined | null) {
		this.mWorkspaceRoot = value
	}

	// /**
	//  * Gets workspace folders.
	//  *
	//  * @readonly
	//  * @type {(WorkspaceFolder[] | undefined)}
	//  * @memberof RegionTreeProvider
	//  */
	// public get workspaceFolders(): WorkspaceFolder[] | undefined {
	// 	return this.mWorkspaceFolders
	// }

	// /**
	//  * Sets workspace folders.
	//  *
	//  * @param {(WorkspaceFolder[] | undefined)} value Value.
	//  *
	//  * @memberof RegionTreeProvider
	//  */
	// public set workspaceFolders(value: WorkspaceFolder[] | undefined) {
	// 	this.mWorkspaceFolders = value
	// }

	/**
	 * Gets data.
	 *
	 * @readonly
	 * @type {(RegionTreeItemArray | undefined)}
	 * @memberof RegionTreeProvider
	 */
	public get data(): RegionTreeItemArray | undefined {
		return this.mData
	}

	/**
	 * Sets data.
	 *
	 * @param {(RegionTreeItemArray | undefined)} value Value.
	 *
	 * @memberof RegionTreeProvider
	 */
	public set data(value: RegionTreeItemArray | undefined) {
		this.mData = value
	}

	/**
	 * Gets auto refresh.
	 *
	 * @readonly
	 * @type {boolean}
	 * @memberof RegionTreeProvider
	 */
	public get autoRefresh(): boolean | undefined {
		return this.mAutoRefresh
	}

	/**
	 * Sets auto refresh.
	 *
	 * @param {boolean} value Value.
	 *
	 * @memberof RegionTreeProvider
	 */
	public set autoRefresh(value: boolean | undefined) {
		this.mAutoRefresh = value
	}

	/**
	 * Gets text editor.
	 *
	 * @readonly
	 * @type {TextEditor}
	 * @memberof RegionTreeProvider
	 */
	public get editor(): TextEditor | undefined {
		return this.mEditor
	}

	/**
	 * Sets text editor.
	 *
	 * @param {TextEditor} value Value.
	 *
	 * @memberof RegionTreeProvider
	 */
	public set editor(value: TextEditor | undefined) {
		this.mEditor = value
	}

	/**
	 * Gets text.
	 *
	 * @readonly
	 * @type {string}
	 * @memberof RegionTreeProvider
	 */
	public get text(): string | undefined {
		return this.mText
	}

	/**
	 * Sets text.
	 *
	 * @param {string} value Value.
	 *
	 * @memberof RegionTreeProvider
	 */
	public set text(value: string | undefined) {
		this.mText = value
	}

	/**
	 * Gets extension context.
	 *
	 * @readonly
	 * @type {(ExtensionContext | undefined)}
	 * @memberof RegionTreeProvider
	 */
	public get context(): ExtensionContext | undefined {
		return this.mContext
	}

	/**
	 * Sets extension context.
	 *
	 * @param {(ExtensionContext | undefined)} value Value.
	 *
	 * @memberof RegionTreeProvider
	 */
	public set context(value: ExtensionContext | undefined) {
		this.mContext = value
	}

	private onActiveEditorChanged(): void {
		if (window.activeTextEditor) {
			const config: IConfiguration = workspace.getConfiguration()
				.get(Consts.EXTENSION_NAMESPACE) as IConfiguration

			const regionViewerEnabled = config['enableRegionViewer'] ?? false
			const regionExplorerEnabled = config['enableRegionExplorer'] ?? false

			if (window.activeTextEditor.document.uri.scheme === 'file') {
				// const enabled = window.activeTextEditor.document.languageId === 'json' ||
				// 	window.activeTextEditor.document.languageId === 'jsonc'

				// commands.executeCommand('setContext', 'regionViewerEnabled', enabled)

				commands.executeCommand('setContext', 'regionViewerEnabled', regionViewerEnabled)
				commands.executeCommand('setContext', 'regionExplorerEnabled', regionExplorerEnabled)

				if (regionViewerEnabled) {
					this.refresh()
				}
			}
		} else {
			commands.executeCommand('setContext', 'regionViewerEnabled', false)
			commands.executeCommand('setContext', 'regionExplorerEnabled', false)
		}
	}

	private onDocumentChanged(changeEvent: TextDocumentChangeEvent): void {
		if (this.autoRefresh && changeEvent.document.uri.toString() === this.editor?.document.uri.toString()) {
			for (const change of changeEvent.contentChanges) {
				// const path = json.getLocation(
				// 	this.text as string,
				// 	this.editor.document.offsetAt(change.range.start)
				// ).path
				// path.pop()
				// const node = path.length ? json.findNodeAtLocation(this.tree, path) : void 0
				// this.parseTree()
				// this._onDidChangeTreeData.fire(node ? node.offset : void 0)
				console.log(change)
				this._onDidChangeTreeData.fire()
			}
		}
	}

	// Drag and drop controller

	public async onDrop(sources: RegionTreeItemArray, target: RegionTreeItem): Promise<void> {
		console.log(sources)
		console.log(target)
	}

	dispose(): void {
		// console.log('destroy')
		logger.info('Disposed')
		Log.info('Disposed')
	}
}

