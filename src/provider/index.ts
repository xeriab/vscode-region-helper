//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Exports
//

'use strict'

export {
	RegionTreeProvider
} from './RegionTreeProvider'

export {
	FoldingRangeProvider
} from './FoldingRangeProvider'

//
// endregion Exports
//
