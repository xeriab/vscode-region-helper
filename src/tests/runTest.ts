//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: runTest.ts
//

//
// region Imports
//

'use strict'

import * as path from 'path'
import {
	runTests,
	// resolveCliPathFromVSCodeExecutablePath,
} from 'vscode-test'
import { Log, format } from '@Ex/utils'
import { logger } from '@Ex/extension'

//
// endregion Imports
//

const main = async (): Promise<void> => {
	try {
		// The folder containing the Extension Manifest package.json
		// Passed to `--extensionDevelopmentPath`
		const extensionDevelopmentPath = path.resolve(__dirname, '../../')
		Log.d(extensionDevelopmentPath)
		logger.d(extensionDevelopmentPath)

		// The path to the extension test script
		// Passed to --extensionTestsPath
		const extensionTestsPath = path.resolve(__dirname, './suite/index')
		Log.d(extensionTestsPath)
		logger.d(extensionTestsPath)

		Log.d(format('Hello {0}. The weather is currently {1}°.', 'Jane', '35'))
		logger.d(format('Hello {0}. The weather is currently {1}°.', 'Jane', '35'))

		// Download VS Code, unzip it and run the integration test
		// await runTests({ extensionDevelopmentPath, extensionTestsPath })
		runTests({ extensionDevelopmentPath, extensionTestsPath })
	} catch (err) {
		// Log.dir(err)
		// Log.dir(err)
		console.error(err)
		console.error('Failed to run tests')
		Log.e('Failed to run tests')
		process.exit(1)
	}
}

main()
