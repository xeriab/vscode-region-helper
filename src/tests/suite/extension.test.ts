//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//
// NOTE: This example test is leveraging the Mocha test framework.
//
// Please refer to their documentation on https://mochajs.org/ for help.
//

//
// @filename: extension.test.ts
//

//
// region Imports
//

'use strict'

// The module 'assert' provides assertion methods from node
import * as assert from 'assert'
// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import { window } from 'vscode'
import { suite as TestSuite, test as TestCase, after } from 'mocha'
import * as path from 'path'
// import * as MyExtension from '../../extension'
import { Log } from '../../utils'

//
// endregion Imports
//

// Defines a Mocha test suite to group tests of similar kind together
TestSuite('Extension Test Suite', () => {
	after(() => {
		window.showInformationMessage('Start all tests.')

		const fileOne = path.resolve(__dirname, './fixtures/test.env')

		Log.d('File One:', fileOne)

		// Log.d(MyExtension)
	})

	// Defines a Mocha unit test
	TestCase('Something 1', () => {
		assert.strictEqual(-1, [1, 2, 3].indexOf(5))
		assert.strictEqual(-1, [1, 2, 3].indexOf(0))
	})
})
