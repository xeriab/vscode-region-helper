//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Imports
//

'use strict'

import * as path from 'path'
import Mocha from 'mocha'
import glob from 'glob'
import { Log } from '@Ex/utils'

//
// endregion Imports
//

export const run = (): Promise<void> => {
	// Create the mocha test
	const mocha = new Mocha({
		// ui: 'bdd',
		ui: 'tdd',
		color: true,
	})

	const testsRoot = path.resolve(__dirname, '..')

	return new Promise((c, e) => {
		glob('**/**.test.js', { cwd: testsRoot }, (err, files) => {
			if (err) {
				return e(err)
			}

			// Add files to the test suite
			files.forEach((f: any) => mocha.addFile(path.resolve(testsRoot, f)))

			try {
				// Run the mocha test
				mocha.run((failures: any) => {
					if (failures > 0) {
						e(new Error(`${failures} tests failed.`))
					} else {
						c()
					}
				})
			} catch (err) {
				Log.e(err)
				e(err)
			}
		})
	})
}
