//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: Log.ts
//

//
// region Imports
//

'use strict'

import * as Consts from '../Constants'
// import * as path from 'path'

//
// endregion Imports
//

const ConsoleMethods = {
	log: console.log.bind(console),
	info: console.info.bind(console),
	warn: console.warn.bind(console),
	error: console.error.bind(console),
	dir: console.dir.bind(console),
	debug: (console.debug || console.log).bind(console)
}

//
// region PrettyConsole
//

/**
 *
 */
export const PrettyConsole: {
	write(...parameters: string[]): void
	style(...parameters: string[]): string
} = {
	write(...parameters: string[]) {
		const strings = parameters.map((parameter: string) => {
			let retVal = parameter

			const texts = [
				'reset',
				'bright',
				'dim',
				'underscore',
				'blink',
				'reverse',
				'hidden',
				'fgBlack',
				'fgRed',
				'fgGreen',
				'fgYellow',
				'fgBlue',
				'fgMagenta',
				'fgCyan',
				'fgWhite',
				'bgBlack',
				'bgRed',
				'bgGreen',
				'bgYellow',
				'bgBlue',
				'bgMagenta',
				'bgCyan',
				'bgWhite',

				'fgLightGreen',
			]

			const codes = [
				'\x1b[0m',
				'\x1b[1m',
				'\x1b[2m',
				'\x1b[4m',
				'\x1b[5m',
				'\x1b[7m',
				'\x1b[8m',
				'\x1b[30m',
				'\x1b[31m',
				'\x1b[32m',
				'\x1b[33m',
				'\x1b[34m',
				'\x1b[35m',
				'\x1b[36m',
				'\x1b[37m',
				'\x1b[40m',
				'\x1b[41m',
				'\x1b[42m',
				'\x1b[43m',
				'\x1b[44m',
				'\x1b[45m',
				'\x1b[46m',
				'\x1b[47m',

				'\x1b[92m',
			]

			for (let x = 0; x < texts.length; ++x) {
				retVal = retVal.replace(RegExp(`{${texts[x]}}`, 'gi'), codes[x])
			}

			return retVal
		})

		if (strings.length) {
			strings[strings.length - 1] = `${strings[strings.length - 1]}\x1b[0m`
		}

		console.log.apply(console, [...strings])
	},
	style(...parameters: string[]): string {
		const strings = parameters.map((parameter: string) => {
			let retVal = parameter

			const texts = [
				'reset',
				'bright',
				'dim',
				'underscore',
				'blink',
				'reverse',
				'hidden',
				'fgBlack',
				'fgRed',
				'fgGreen',
				'fgYellow',
				'fgBlue',
				'fgMagenta',
				'fgCyan',
				'fgWhite',
				'bgBlack',
				'bgRed',
				'bgGreen',
				'bgYellow',
				'bgBlue',
				'bgMagenta',
				'bgCyan',
				'bgWhite',

				'fgLightGreen',
			]

			const codes = [
				'\x1b[0m',
				'\x1b[1m',
				'\x1b[2m',
				'\x1b[4m',
				'\x1b[5m',
				'\x1b[7m',
				'\x1b[8m',
				'\x1b[30m',
				'\x1b[31m',
				'\x1b[32m',
				'\x1b[33m',
				'\x1b[34m',
				'\x1b[35m',
				'\x1b[36m',
				'\x1b[37m',
				'\x1b[40m',
				'\x1b[41m',
				'\x1b[42m',
				'\x1b[43m',
				'\x1b[44m',
				'\x1b[45m',
				'\x1b[46m',
				'\x1b[47m',

				'\x1b[92m',
			]

			for (let x = 0; x < texts.length; ++x) {
				retVal = retVal.replace(RegExp(`{${texts[x]}}`, 'gi'), codes[x])
			}

			return retVal
		})

		if (strings.length) {
			strings[strings.length - 1] = `${strings[strings.length - 1]}\x1b[0m`
		}

		// console.log.apply(console, [...strings])

		// let retStr = strings.join('')

		return strings.join('')
	},
}

//
// endregion PrettyConsole
//

//
// region Log
//

export class Log {
	/**
	 * Log Domain.
	 *
	 * @private
	 * @static
	 * @type {(String|null)}
	 * @memberof Log
	 */
	private static sLogDomain: string | null = `${Consts.EXTENSION_NAME}`

	/**
	 * Log.
	 *
	 * @private
	 * @static
	 * @type {(Log|null)}
	 * @memberof Log
	 */
	private static sInstance: Log | null = null

	/**
	 * Log Constructor.
	 *
	 * @param {(String|null)} prefix Log Domain.
	 */
	constructor(prefix: string | null = Consts.EXTENSION_NAME) {
		if (Log.sLogDomain === null) {
			Log.sLogDomain = prefix
		}

		if (Log.sInstance === null) {
			Log.sInstance = this
		}

		// Log.sLogDomain = prefix
	}

	/**
	 * Get Log Domain.
	 *
	 * @static
	 * @type {(String|null)}
	 * @memberof Log
	 *
	 * @return {(String|null)} - Log Domain.
	 */
	public static get logDomain(): string | null {
		return Log.sLogDomain
	}

	/**
	 * Set Log Domain.
	 *
	 * @param {(String|null)} value - Log prefix to set.
	 *
	 * @static
	 * @memberof Log
	 */
	public static set logDomain(value: string | null) {
		if (Log.sLogDomain === null) {
			Log.sLogDomain = value
		} else if (Log.sLogDomain !== value) {
			Log.sLogDomain = value
		}
	}

	static dir(object: any, options?: any): void {
		console.dir.apply(console, [object, options])
	}

	static debug(...parameters: any): void {
		// const prefix = `[${Log.logDomain}][DEBUG]:`
		// const prefix = PrettyConsole.style(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgLightGreen}[{reset}{fgGreen}DEBUG{reset}{dim}{fgLightGreen}]{reset}{dim}{fgWhite}:{reset}`)
		// ConsoleMethods.debug.apply(console, [prefix, ...parameters])
		// PrettyConsole.write(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgLightGreen}[{reset}{fgGreen}DEBUG{reset}{dim}{fgLightGreen}]{reset}{dim}{fgWhite}:{reset}`)
		// ConsoleMethods.debug.apply(console.debug, [...parameters])
		const prefix = PrettyConsole.style(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgLightGreen}[{reset}{fgGreen}DEBUG{reset}{dim}{fgLightGreen}]{reset}{dim}{fgWhite}:{reset}`)
		ConsoleMethods.debug.apply(console, [prefix, ...parameters])
	}

	static error(...parameters: any): void {
		// const prefix = `[${Log.logDomain}][ERROR]:`
		const prefix = PrettyConsole.style(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgRed}[{reset}{fgRed}ERROR{reset}{dim}{fgRed}]{reset}{dim}{fgWhite}:{reset}`)
		console.error.apply(console, [prefix, ...parameters])
	}

	static log(...parameters: any): void {
		// const prefix = `[${Log.logDomain}][LOG]:`
		const prefix = PrettyConsole.style(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgCyan}[{reset}{fgCyan}LOG{reset}{dim}{fgCyan}]{reset}{dim}{fgWhite}:{reset}`)
		console.log.apply(console, [prefix, ...parameters])
	}

	static info(...parameters: any): void {
		// const prefix = `[${Log.logDomain}][INFO]:`
		const prefix = PrettyConsole.style(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgBlue}[{reset}{fgBlue}INFO{reset}{dim}{fgBlue}]{reset}{dim}{fgWhite}:{reset}`)
		console.info.apply(console.info, [prefix, ...parameters])
	}

	static warn(...parameters: any): void {
		// const prefix = `[${Log.logDomain}][WARN]:`
		const prefix = PrettyConsole.style(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgYellow}[{reset}{fgYellow}WARN{reset}{dim}{fgYellow}]{reset}{dim}{fgWhite}:{reset}`)
		console.warn.apply(console, [prefix, ...parameters])
	}

	static verbose(...parameters: any): void {
		// const prefix = `[${Log.logDomain}][VERBOSE]:`
		const prefix = PrettyConsole.style(`{dim}{fgWhite}[{reset}{bright}${Log.logDomain}{reset}{dim}{fgWhite}]{reset}{dim}{fgCyan}[{reset}{fgCyan}VERBOSE{reset}{dim}{fgCyan}]{reset}{dim}{fgWhite}:{reset}`)
		console.log.apply(console, [prefix, ...parameters])
	}

	static d(...parameters: any): void {
		Log.debug(...parameters)
	}

	static e(...parameters: any): void {
		Log.error(...parameters)
	}

	static l(...parameters: any): void {
		Log.log(...parameters)
	}

	static i(...parameters: any): void {
		Log.info(...parameters)
	}

	static w(...parameters: any): void {
		Log.warn(...parameters)
	}

	static v(...parameters: any): void {
		Log.verbose(...parameters)
	}

	static getInstance(prefix: string | null = Consts.EXTENSION_NAME): Log {
		if (Log.sInstance === null) {
			Log.sInstance = new Log(prefix)
		}

		return Log.sInstance
	}
}

//
// endregion Log
//
