//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: Logging.ts
//

//
// region Imports
//

'use strict'

import { OutputChannel, window } from 'vscode'
import * as Os from 'os'
import * as Util from 'util'
import { createLogger, Logger, format, transports } from 'winston'
import Transport from 'winston-transport'
import { isNotEmpty, isString } from './Misc'
import * as Consts from './../Constants'
// import { isEmpty } from 'underscore'

//
// endregion Imports
//

//
// region Logging
//

class VSCodeExtensionTransport extends Transport {
	constructor(
		private readonly outputChannel: OutputChannel | string | null
	) {
		super()
	}

	log(info: any, next: () => void) {
		setImmediate(() => {
			this.emit('logged', info)
		})

		if (this.outputChannel !== null && typeof this.outputChannel === 'object') {
			this.outputChannel.appendLine(info.message)
		}

		if (next) {
			next()
		}
	}
}

export class Logging {
	/**
	 * Logger.
	 *
	 * @private
	 * @readonly
	 * @type {(Logger|null)}
	 * @memberof Logging
	 */
	private mLogger!: Logger

	/**
	 * Log Domain.
	 *
	 * @private
	 * @type {(String|null)}
	 * @memberof Logging
	 */
	protected mLogDomain: string | null = `${Consts.EXTENSION_NAME}`

	protected static sInstance: Logging | null = null

	constructor(
		outputChannel: OutputChannel,
		logLevel: string | null = 'info'
	) {
		const { combine, metadata, printf } = format

		// // Ignore log messages if they have { private: true }
		// const ignorePrivate = format((info, _options) => {
		// 	if (info.private) { return false }
		// 	return info
		// })

		this.mLogger = createLogger({
			level: logLevel || 'info',
			format: combine(
				// ignorePrivate(),
				format.splat(),
				format.simple(),
				format.json(),
				format.colorize(),
				metadata(),
				printf(info => {
					let result = `${info.level}: `

					// tslint:disable-next-line: strict-type-predicates
					if (isString(info.message)) {
						result += info.message
					} else {
						result += Util.inspect(info.message, false, 5)
					}

					if (info.metadata && isNotEmpty(info.metadata)) {
						result += Os.EOL

						if (isString(info.metadata)) {
							result += info.metadata
						} else {
							result += Util.inspect(info.metadata, false, 5)
						}
					}

					return result
				}),
			),
			transports: [
				new VSCodeExtensionTransport(outputChannel),
				new transports.Console({
					format: format.simple()
				}),
			],
			exitOnError: false,
		})

		if (Logging.sInstance === null) {
			Logging.sInstance = this
		}
	}

	/**
	 * Get Log Domain.
	 *
	 * @type {(String|null)}
	 * @memberof Logging
	 *
	 * @return {(String|null)} - Log Domain.
	 */
	public get logDomain(): string | null {
		return this.mLogDomain
	}

	/**
	 * Set Log Domain.
	 *
	 * @param {(String|null)} value - Log domain to set.
	 *
	 * @memberof Logging
	 */
	public set logDomain(value: string | null) {
		if (this.mLogDomain !== value) {
			this.mLogDomain = value
		}
	}

	public write(level: string, ...parameters: any): void {
		this.mLogger.log(level, [...parameters])
	}

	public log(message: any, context?: any): void {
		this.mLogger.info(message, context)
	}

	// /**
	//  * @override
	//  */
	// public log(...parameters: any): void {
	// 	return this.write('info', ...parameters)
	// }

	public error(message: any, context?: any): void {
		this.mLogger.error(message, context)
	}

	public warn(message: any, context?: any): void {
		this.mLogger.warn(message, context)
	}

	public info(message: any, context?: any): void {
		this.mLogger.info(message, context)
	}

	public debug(message: any, context?: any): void {
		this.mLogger.debug(message, context)
	}

	// public debug(message: string, ...parameters: any): void {
	// 	this.mLogger.debug(message, ...parameters)
	// }

	public notice(message: any, context?: any): void {
		this.mLogger.notice(message, context)
	}

	public crit(message: any, context?: any): void {
		this.mLogger.crit(message, context)
	}

	public alert(message: any, context?: any): void {
		this.mLogger.alert(message, context)
	}

	public emerg(message: any, context?: any): void {
		this.mLogger.emerg(message, context)
	}

	public d(message: any, context?: any): void {
		this.debug(message, context)
	}

	public l(message: any, context?: any): void {
		this.log(message, context)
	}

	public e(message: any, context?: any): void {
		this.error(message, context)
	}

	public w(message: any, context?: any): void {
		this.warn(message, context)
	}

	public i(message: any, context?: any): void {
		this.info(message, context)
	}

	public n(message: any, context?: any): void {
		this.notice(message, context)
	}

	public c(message: any, context?: any): void {
		this.crit(message, context)
	}

	public a(message: any, context?: any): void {
		this.alert(message, context)
	}

	static register(extensionName: string, logLevel = 'info') {
		const outputChannel = window.createOutputChannel(
			// extensionName ?? Consts.EXTENSION_NAME
			extensionName ? extensionName : Consts.EXTENSION_NAME
		)

		if (Logging.sInstance === null) {
			Logging.sInstance = new Logging(outputChannel, logLevel)
		}

		// return new Logging(outputChannel, logLevel)
		return Logging.sInstance
	}

	static getInstance(
		logDomain: string | null = Consts.EXTENSION_NAME,
		logLevel: string | null = 'info'
	): Logging {
		const outputChannel = window.createOutputChannel(
			logDomain ? logDomain : Consts.EXTENSION_NAME
		)

		if (Logging.sInstance === null) {
			Logging.sInstance = new Logging(outputChannel, logLevel)
		}

		// return new Logging(outputChannel, logLevel)
		return Logging.sInstance
	}
}

//
// endregion Logging
//
