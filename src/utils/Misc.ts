//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: Misc.ts
//

'use strict'

//
// region Imports
//

// import { OutputChannel, window } from 'vscode'
// import { EOL } from 'os'
// import { isEmpty } from 'underscore'
// import { inspect } from 'util'
// import { createLogger, Logger, format, transports } from 'winston'
// import Transport from 'winston-transport'
import * as Consts from '../Constants'
import {
	ILanguageDelimitersConfig,
	IRegionDelimiter
} from '../config'

//
// endregion Imports
//

//
// region Misc.
//

//
const tagTester = (name: string): any => {
	const tag = '[object ' + name + ']'
	return (object: any) => Object.prototype.toString.call(object) === tag
}

//
const has = (object: any, key: string): boolean => {
	return object !== null && Object.prototype.hasOwnProperty.call(object, key)
}

//
const shallowProperty = (key: any) => {
	return (object: any) => object == null ? void 0 : object[key]
}

// Is a given value an array?
// Delegates to ECMA5's native `Array.isArray`.
export const isArray = Array.isArray || tagTester('Array')

//
export const isString = tagTester('String')

//
export const isNumber = tagTester('Number')

//
export const isBoolean = tagTester('Boolean')

//
export const isBool = tagTester('Boolean')

//
export const isRegExp = tagTester('RegExp')

//
let _isArguments: any = tagTester('Arguments');

// Define a fallback version of the method in browsers (ahem, IE < 9), where
// there isn't any inspectable "Arguments" type.
// eslint-disable-next-line no-unexpected-multiline
(function () {
	// eslint-disable-next-line prefer-rest-params
	if (!_isArguments(arguments)) {
		_isArguments = (object: any) => has(object, 'callee')
	}
}());

//
export const isArguments$1 = _isArguments

//
export const isArguments = _isArguments

// // Internal helper to obtain the `byteLength` property of an object.
// const getByteLength = shallowProperty('byteLength')

// Internal helper to obtain the `length` property of an object.
const getLength = shallowProperty('length')

/**
 * Returns true if `collection` contains no values.
 *
 * For strings and array-like objects checks if the length property is 0.
 *
 * @param {Any} collection The collection to check.
 *
 * @returns {Boolean} `true` if the given `collection` has no elements, `false`
 * 					  otherwise.
 */
export const isEmpty = (collection: any): boolean => {
	let retValue = false

	// Skip the more expensive `toString`-based type checks if `obj` has no
	// `.length`.
	const length = getLength(collection)

	if (collection === null) {
		retValue = true
	}

	// if (typeof collection === 'string' && collection === '') {
	// 	retValue = true
	// } else if (typeof collection === 'object' && collection.length === 0) {
	// 	retValue = true
	// } else if (Array.isArray(collection) && collection.length === 0) {
	// 	retValue = true
	// } else {
	// 	retValue = getLength(keys(collection)) === 0
	// }

	if (typeof length === 'number' && (
		isArray(collection) || isString(collection) || isArguments$1(collection)
	)) {
		retValue = length === 0
	} else {
		retValue = getLength(Object.keys(collection)) === 0
	}

	return retValue
}

/**
 * Returns true if `collection` contains values.
 *
 * For strings and array-like objects checks if the length property is not 0.
 *
 * @param {Any} collection The collection to check.
 *
 * @returns {Boolean} `true` if the given `collection` has elements, `false`
 * 					  otherwise.
 */
export const isNotEmpty = (collection: any): boolean => !isEmpty(collection)

export const getRegionDelimiters = (languageId: string, languageDelimitersConfig: ILanguageDelimitersConfig): IRegionDelimiter => {
	const regionDelimiter = languageDelimitersConfig[languageId] || languageDelimitersConfig['default']
	return regionDelimiter
}

export const isColor = (str: string): boolean => {
	if (Consts.CSS_COLOR_NAMES.join(',').toLowerCase().indexOf(str.toLowerCase()) !== -1) {
		return true
	}

	const isHexColor = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i

	return isHexColor.test(str)
}

export const replace = (context: any): any => {
	return function (tag: string, name: string): any {
		if (tag.substring(0, 2) == '{{' && tag.substring(tag.length - 2) == '}}') {
			return '{' + name + '}'
		}

		if (!has(context, name)) {
			return tag
		}

		if (typeof context[name] === 'function') {
			return context[name]()
		}

		return context[name]
	}
}

export const format = (...parameters: any): string => {
	const text: string = parameters[0]
	let context: any = null

	if (typeof parameters[1] === 'object' && parameters[1]) {
		context = parameters[1]
	} else {
		context = Array.prototype.slice.call(parameters, 1)
	}

	return String(text).replace(/\{?\{([^\\{\\}]+)\}\}?/g, replace(context))
}

//
// endregion Misc.
//
