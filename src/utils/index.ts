//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Exports
//

'use strict'

export {
	// isArguments$1,
	isArguments,
	isArray,
	isBool,
	isBoolean,
	isEmpty,
	isNotEmpty,
	isNumber,
	isString,
	format,
} from './Misc'

export {
	Log,
	PrettyConsole,
} from './Log'

export {
	Logging,
} from './Logging'

//
// endregion Exports
//
