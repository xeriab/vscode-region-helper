//
// -*- tab-width: 2; encoding: utf-8; mode: ts; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: index.ts
//

//
// region Imports
//

'use strict'

import {
	EventEmitter,
	Event,
	Command,
	TreeItem,
	// ProviderResult,
	TreeItemCollapsibleState,
	// TreeDataProvider,
	// Range,
	// window,
} from 'vscode'
// import * as Markers from './../Markers.json'
// import * as Consts from './../Constants'
import * as path from 'path'
// import * as fs from 'fs'

//
// endregion Imports
//

export class RegionTreeItem extends TreeItem {
	children: Array<RegionTreeItem> = []

	constructor(
		public readonly label: string,
		private readonly info?: string | boolean | undefined,
		public collapsibleState?: TreeItemCollapsibleState,
		public readonly command?: Command | undefined,
		public lineNo?: number | undefined
	) {
		// super(label, collapsibleState)
		// super(label, TreeItemCollapsibleState.None)
		super(label, collapsibleState ?? TreeItemCollapsibleState.None)

		if (info !== undefined || typeof info !== 'string') {
			this.tooltip = `${this.label} Region`
			this.description = false
		} else {
			// this.tooltip = `${this.label} Region`
			// this.description = false
			this.description = this.info

			if (typeof info === 'string') {
				this.tooltip = `${this.label}-${this.info}`
			}
		}

		// this.command = {
		// 	// title: '',
		// 	// command: 'region-viewer.reveal',
		// 	title: `Reveal region in current document.`,
		// 	command: `${Consts.SIMPLE_NAME}.revealRegion`,
		// 	arguments: [lineNumber],
		// }
	}

	iconPath = {
		light: path.join(__filename, '..', '..', 'resources', 'icons', 'light', 'sharp-icon.svg'),
		dark: path.join(__filename, '..', '..', 'resources', 'icons', 'dark', 'sharp-icon.svg'),
	}

	contextValue = 'region'

	/**
	 * Gets line number.
	 *
	 * @readonly
	 * @type {(number | undefined)}
	 * @memberof RegionTreeItem
	 */
	public get lineNumber(): number | undefined {
		return this.lineNo
	}

	/**
	 * Sets line number.
	 *
	 * @param {(number | undefined)} value Line number.
	 *
	 * @memberof RegionTreeItem
	 */
	public set lineNumber(value: number | undefined) {
		this.lineNo = value
	}
}

export type RegionTreeItemType = RegionTreeItem | undefined | null | void
export type RegionTreeItemArray = Array<RegionTreeItem>
export type RegionTreeItemEventEmitter = EventEmitter<RegionTreeItemType>
export type RegionTreeItemEvent = Event<RegionTreeItemType>
