//
// -*- tab-width: 2; encoding: utf-8; mode: js; -*-
//
// Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
//
// SPDX-License-Identifier: MIT
//

//
// @filename: webpack.config.js
//

// @ts-check

'use strict'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path')

/** @type {import('webpack').Configuration} */
const config = {
    // VSCode extensions run in a Node.js-context,
    // 📖 -> https://webpack.js.org/configuration/node/
    target: 'node',

    // The entry point of this extension,
    // 📖 -> https://webpack.js.org/configuration/entry-context/
    entry: './src/extension.ts',

    // The bundle is stored in the 'dist' folder (check package.json),
    // 📖 -> https://webpack.js.org/configuration/output/
    output: {
        // eslint-disable-next-line no-undef
        path: path.resolve(__dirname, 'out'),
        filename: 'extension.js',
        // libraryTarget: 'commonjs',
        libraryTarget: 'commonjs2',
        // devtoolModuleFilenameTemplate: '../[resource-path]',
        // devtoolModuleFilenameTemplate: 'file:///[absolute-resource-path]'
        devtoolModuleFilenameTemplate: '[absolute-resource-path]'
    },

    // devtool: 'source-map',

    externals: {
        // The vscode-module is created on-the-fly and must be excluded.
        // Add other modules that cannot be webpack'ed,
        // 📖 -> https://webpack.js.org/configuration/externals/
        vscode: 'commonjs vscode',
    },

    // Support reading TypeScript and JavaScript files,
    // 📖 -> https://github.com/TypeStrong/ts-loader
    resolve: {
        extensions: ['.ts', '.js', '.json', '.tsx', 'jsx'],
    },

    module: {
        rules: [{
            test: /\.ts$/,
            exclude: /node_modules/,
            use: [{
                loader: 'ts-loader',
                options: {
                    compilerOptions: {
                        // Override `tsconfig.json` so that TypeScript emits
                        // native JavaScript modules.
                        // 'module': 'es6'
                        // 'module': 'commonjs'
                    }
                }
            }]
        }]
    },
}

if (process.env.NODE_ENVIRONMENT !== 'production') {
    config.devtool = 'source-map'
}

module.exports = config
